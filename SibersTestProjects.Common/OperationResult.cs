﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProjects.Common
{
    public class OperationResult
    {
        public bool Result { get; set; }
        public string Message { get; set; }

        public OperationResult(bool result, string message)
        {
            Result = result;
            Message = message;
        }
    }
}
