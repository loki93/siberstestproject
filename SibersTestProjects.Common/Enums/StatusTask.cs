﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProjects.Common.Enums
{
    public enum StatusTask
    {
        ToDo = 0,
        InProgress = 1,
        Done = 2
    }
}
