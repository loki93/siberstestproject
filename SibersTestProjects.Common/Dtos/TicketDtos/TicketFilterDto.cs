﻿using SibersTestProjects.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProjects.Common.Dtos.TicketDtos
{
    public class TicketFilterDto
    {
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public string StatusName { get; set; }
        public string AuthorName { get; set; }
        public string ExecutingEmployeeName { get; set; }
    }
}
