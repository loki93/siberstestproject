﻿using SibersTestProjects.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProjects.Common.Dtos.TicketDtos
{
    public class CreateTicketDto : BaseEntityDto
    {
        public string Name { get; set; }
        public string AuthorId { get; set; }
        public string ExecutingEmployeeId { get; set; }
        public StatusTask Status { get; set; }
        public string Commentary { get; set; }
        public int Priority { get; set; }
        public int ProjectId { get; set; }
    }
}
