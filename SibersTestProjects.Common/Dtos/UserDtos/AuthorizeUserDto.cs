﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProjects.Common.Dtos.UserDtos
{
    public class AuthorizeUserDto
    {
        public string Id { get; set; }
        public string Token { get; set; }
        public IList<string> Roles { get; set; }
    }
}
