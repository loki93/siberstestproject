﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProjects.Common.Dtos.UserDtos
{
    public class UserFilterDto
    {
        public string Fullname { get; set; }
        public string SelectedRole { get; set; }
    }
}
