﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProjects.Common.Dtos.UserDtos
{
    public class UserManagerDto
    {
        public Guid Id { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public IList<string> Rolenames { get; set; }
    }
}
