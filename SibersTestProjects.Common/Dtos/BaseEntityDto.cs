﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProjects.Common.Dtos
{
    public class BaseEntityDto
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}
