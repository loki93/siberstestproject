﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProjects.Common.Dtos.ProjectDtos
{
    public class ProjectFilterDto
    {
        public string UserId { get; set; }
        public string UserRole { get; set; }
        public string Name { get; set; }
        public string CustomerCompanyName { get; set; }
        public string ExecutingCompanyName { get; set; }
        public string SupervisorProjectName { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
