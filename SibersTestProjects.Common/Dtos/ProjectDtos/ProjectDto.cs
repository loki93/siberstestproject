﻿using SibersTestProjects.Common.Dtos.TicketDtos;
using SibersTestProjects.Common.Dtos.UserDtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProjects.Common.Dtos.ProjectDtos
{
    public class ProjectDto : BaseEntityDto
    {
        public string Name { get; set; }
        public string CustomerCompanyName { get; set; }
        public string ExecutingCompanyName { get; set; }
        public string SupervisorProjectId { get; set; }
        public string SupervisorProjectName { get; set; }

        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Priority { get; set; }
        
        public List<string> ExecutingEmployeesName { get; set; }
        public List<string> ExecutingEmployeesId { get; set; }
        public List<TicketDto> Tickets { get; set; }
        public List<UserManagerDto> Users { get; set; }
    }
}
