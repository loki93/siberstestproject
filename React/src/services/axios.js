import React, { Component } from 'react';
import axios from 'axios';

class Axios extends Component {

    token = localStorage.getItem('token');
    async getToken() {
        while (this.token === null) {
            this.token = await localStorage.getItem('token');
            console.log(this.token)
        }
    };

    instance = axios.create({
        baseURL: 'https://localhost:44302',
        headers: { 'Authorization': `Bearer ${this.token}` },
        withCredentials: true
    });
}


export default Axios;