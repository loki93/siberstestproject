export default class AuthService {
    isAuthenificated() {
        const token = localStorage.getItem('token');
        const roles = localStorage.getItem('roles');
        return roles && token !== null ? true : false;
    }

    login(token, roles) {
        localStorage.roles = roles;
        localStorage.token = token;
    }

    logout() {
        localStorage.removeItem('roles');
        localStorage.removeItem('token');
    }

    passwordValidator = (password) => {
        return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/.test(password);
    }

    checkForRoles() {
        return localStorage.getItem('roles');
    }

    setAuthorizeData = (token, roles) => {
        localStorage.setItem('roles', roles);
        localStorage.setItem('token', token)
    }

    setUserId = (id) => {
        localStorage.setItem('id', id)
    }
}