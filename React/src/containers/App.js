import React, {PureComponent} from 'react';
import {Route, Switch, HashRouter} from 'react-router-dom'
import '../styles/bootstrap.css';
import AuthService from '../services/AuthService';
import SignIn from '../Pages/Authorization/SignIn';
import Home from '../Pages/Home/Home';
import UserManager from '../Pages/SupervisorPages/UserManager';
import CreateUserPage from '../Pages/UserPages/CreateUserPage';
import EditUserPage from '../Pages/UserPages/EditUserPage';
import ProjectManager from '../Pages/ProjectPages/ProjectManager';
import CreateProjectPage from '../Pages/ProjectPages/CreateProjectPage';
import EditProjectPage from '../Pages/ProjectPages/EditProjectPage';
import ProjectDetails from '../Pages/ProjectPages/ProjectDetails';
import CreateTicketPage from '../Pages/TicketPages/CreateTicketPage';
import EditTicketPage from '../Pages/TicketPages/EditTicketPage';

class App extends PureComponent {

  constructor(props) {
    super(props)
    this.authService = new AuthService();
  }

  render() {
    let SupervisorRoute;
    let ManagerRoute;
    let AllRoute = [
      <Route path="/" exact component={SignIn} key={'signin'}/>,
      <Route path="/home" exact component={Home} key={'home'}/>
    ]

    SupervisorRoute = [
      //UserManager
      <Route path="/usermanager" exact component={UserManager} key={'UserManager'}/>,
      <Route path="/adduser" exact component={CreateUserPage} key={"AddUser"} />,
      <Route path="/edituser" component={EditUserPage} key={"EditUser"} />,

      //ProjectManager
      <Route path="/projects" exact component={ProjectManager} key={'ProjectManager'}/>,
      <Route path="/addproject" exact component={CreateProjectPage} key={'AddProject'}/>,
      <Route path="/editproject" component={EditProjectPage} key={'EditProject'}/>,
      <Route path="/project" component={ProjectDetails} key={'ProjectDetails'} />,
      <Route path="/addticket" component={CreateTicketPage} key={'AddTicket'} />,
      <Route path="/editticket" component={EditTicketPage} key={'EditTicket'} />
    ]

    ManagerRoute = [
      <Route path="/projects" exact component={ProjectManager} key={'ProjectManager'}/>
    ]

    return (
      <HashRouter>
        <Switch>
          {AllRoute}
          {SupervisorRoute}
          {ManagerRoute}
        </Switch>
      </HashRouter>
    )
  }
  
}

export default App;
