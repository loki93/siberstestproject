export default theme => ({
    card: {
        maxWidth: 275,
        marginBottom: 20,
        marginRight: 20
      },
      title: {
        fontSize: 24,
      },
      pos: {
        marginBottom: 12,
      }
});