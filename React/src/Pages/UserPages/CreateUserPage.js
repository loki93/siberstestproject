import React from 'react';
import Menu from '../../components/Menu';
import AuthService from '../../services/AuthService';
import axios from '../../services/axios';
import CreateFormTheme from '../../styles/CreatePageStyles';
import generatePassword from 'generate-password';
import CustomSnackBar from '../../components/SnackBar';
import { Redirect } from 'react-router-dom';
import { Container, CssBaseline, Avatar, Typography, Grid, TextField, FormControl, InputLabel, Select, Input, MenuItem, Checkbox, ListItemText, FormHelperText, InputAdornment, IconButton, Button } from '@material-ui/core';
import Settings from '@material-ui/icons/Settings';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';


class CreateUserPage extends React.Component {
    constructor(props) {
        super(props);
        this.authService = new AuthService();
        this.axios = new axios();
        this.state = {
            roles: [],
            selectedRoles: [],
            firstName: '',
            surname: '',
            middlename: '',
            email: '',
            password: '',
            showPassword: true,
            requestStatus: false,
            roleError: ''
        }
    }

    uri = this.props.location.pathname.split('/');

    componentDidMount() {
        this.axios.instance.get('/api/roles')
            .then(response => this.setState({ roles: response.data }))
            .catch(error => console.log('Error:' + error));
    }

    handleChange(event) {
        event.preventDefault();
        if(event.target.name === 'selectedRole') {
            this.setState({ roleError: '' });
        }
        this.setState({ [event.target.name]: event.target.value });
    }

    generatePassword = () => {
        var password = generatePassword.generate({
            length: 6,
            numbers: true,
            strict: true
        });
        this.setState({ password: password});
    }

    handleClickShowPassword = () => {
        this.setState({ showPassword: !this.state.showPassword });
    }

    handleMouseDownPassword = event => {
        event.preventDefault();
    }

    handleClick = event => {
        event.preventDefault();
        this.props.history.push('/usermanager');
    }

    submitForm(event) {
        event.preventDefault();
        if (this.authService.passwordValidator(this.state.password) === true) {
          if (this.state.selectedRoles.length > 0) {
            this.axios.instance.post('/api/adduser', {
              name: this.state.firstName,
              surname: this.state.surname,
              email: this.state.email,
              password: this.state.password,
              roles: this.state.selectedRoles,
              middlename: this.state.middlename,
            })
            .then(response => this.setState({snackbarMessage:'Изменения сохранены. Вы будете перенаправлены.', 
            snackbarVariant: 'success'}, () => this.refs.child.handleOpen(), setTimeout(() => this.setState({ requestStatus: true }), 2000)))
              .catch(error =>
                this.setState({
                  snackbarMesssage: 'Данный email уже используется',
                  snackbarVariant: 'error'
                }, () => this.refs.child.handleOpen()))
          }
          else {
            this.setState({ roleError: 'Выберите роль' })
          }
        }
        else {
          this.setState({
            passwordError: 'Пароль должен состоять  как минимум  из 6 латинских символов,' +
              'из которых: 1 строчный,1 заглавный и 1 цифра', errorPass: true
          });
        }
      }

    render() {
        const isAuthentificated = this.authService.isAuthenificated();
        const { classes } = this.props;
        const { firstName, surname, middlename, email, password, selectedRoles,
            passwordError, errorPass, showPassword, requestStatus, roleError, snackbarMessage, snackbarVariant} = this.state;

        return (
            !isAuthentificated ? <Redirect to="/" /> : (
                requestStatus ? <Redirect to='/usermanager' /> : (
                    <div>
                        <Menu />
                        <CustomSnackBar ref="child" message={snackbarMessage} variant={snackbarVariant} />
                        <Container component="main" maxWidth="xs">
                            <CssBaseline />
                            <div className={classes.papper}>
                                <Avatar className={classes.avatar}>
                                    <Settings />
                                </Avatar>
                                <Typography component="h1" variant="h5">
                                    Добавление пользователя
                                </Typography>
                                <form className={classes.form} onSubmit={this.submitForm.bind(this)}>
                                    <Grid container spacing={1}>
                                        <Grid item xs={12} sm={6}>
                                            <TextField name="firstName"
                                                       variant="outlined"
                                                       value={firstName}
                                                       required
                                                       fullWidth
                                                       id="firstName"
                                                       onChange={this.handleChange.bind(this)}
                                                       label="Имя"
                                                       autoFocus
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField name="surname"
                                                       variant="outlined"
                                                       value={surname}
                                                       required
                                                       fullWidth
                                                       id="surname"
                                                       onChange={this.handleChange.bind(this)}
                                                       label="Фамилия"
                                                       
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField name="middlename"
                                                       variant="outlined"
                                                       value={middlename}
                                                       fullWidth
                                                       id="middlename"
                                                       onChange={this.handleChange.bind(this)}
                                                       label="Отчество"
                                                       
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField name="email"
                                                       variant="outlined"
                                                       value={email}
                                                       required
                                                       fullWidth
                                                       id="email"
                                                       type="email"
                                                       onChange={this.handleChange.bind(this)}
                                                       label="Email(Логин)"
                                                       
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormControl className={classes.formControl} fullWidth required>
                                                <InputLabel htmlFor="select-multiple-checkbox">Роль</InputLabel>
                                                <Select multiple
                                                        value={selectedRoles}
                                                        name="selectedRoles"
                                                        onChange={this.handleChange.bind(this)}
                                                        input={ <Input id="select-multiple-checkbox" /> }
                                                        renderValue={selected => selected.join(', ')}
                                                >
                                                    {this.state.roles.map(role => 
                                                        <MenuItem key={role} value={role}>
                                                            <Checkbox checked={ selectedRoles.indexOf(role) > -1 } />
                                                            <ListItemText primary={role}/>
                                                        </MenuItem>
                                                    )}
                                                </Select>
                                                <FormHelperText error>{roleError}</FormHelperText>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={7}>
                                            <TextField name="password"
                                                       variant="outlined"
                                                       required
                                                       fullWidth
                                                       label="Пароль"
                                                       id="password"
                                                       value={password}
                                                       type={showPassword ? 'text' : 'password'}
                                                       helperText={passwordError}
                                                       error={errorPass}
                                                       onChange={this.handleChange.bind(this)}
                                                       InputProps={{
                                                           endAdornment: (
                                                               <InputAdornment position="end">
                                                                   <IconButton edge="end"
                                                                               aria-label="toggle password visibility"
                                                                               onClick={this.handleClickShowPassword}
                                                                               onMouseDown={this.handleMouseDownPassword}
                                                                    >
                                                                        {showPassword ? <VisibilityOff /> : <Visibility />}
                                                                    </IconButton>
                                                               </InputAdornment>
                                                           )
                                                        }}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={5}>
                                            <Button fullWidth
                                                    variant="contained"
                                                    onClick={this.generatePassword}
                                                    color="primary"
                                                    className={classes.generate}
                                            >
                                                Сгенерировать
                                            </Button>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Button fullWidth
                                                    type="submit"
                                                    color="primary"
                                                    variant="contained"
                                                    className={classes.submit}
                                            >
                                                Добавить
                                            </Button>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Button fullWidth
                                                    onClick={this.handleClick}
                                                    variant="contained"
                                                    className={classes.submit}
                                            >
                                                Отменить
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </form>
                            </div>
                        </Container>
                    </div>
                )
            )
        )
    }
}
CreateUserPage.propTypes = {
    classes: PropTypes.object.isRequired
};
export default withStyles(CreateFormTheme)(CreateUserPage);
