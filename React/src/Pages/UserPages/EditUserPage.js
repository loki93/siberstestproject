import React from 'react';
import axios from '../../services/axios';
import AuthService from '../../services/AuthService';
import { withStyles } from '@material-ui/core/styles';
import Menu from '../../components/Menu';
import CustomSnackBar from '../../components/SnackBar';
import EditFormTheme from '../../styles/CreatePageStyles';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Container, CssBaseline, Avatar, Typography, Grid, TextField, FormControl, InputLabel, Select, Input, MenuItem, Checkbox, ListItemText, FormHelperText, Button } from '@material-ui/core';
import Settings from '@material-ui/icons/Settings';

class EditUserPage extends React.Component {
    constructor(props) {
        super(props);
        this.authService = new AuthService();
        this.axios = new axios();
        this.state = {
          id: '',
          roles: [],
          selectedRoles: [],
          firstName: '',
          surname: '',
          middlename: '',
          email: '',
          password: '',
          requestStatus: false,
          roleError: '',
          helperStatus: ''
        }
    }

    uri = this.props.location.pathname.split('/');
    componentDidMount() {
        this.axios.instance.get('api/user/' + this.uri[2])
          .then(response => this.setState({
            id: response.data.id,
            email: response.data.email,
            roles: response.data.roles,
            password: response.data.password,
            surname: response.data.surname,
            middlename: response.data.middlename,
            firstName: response.data.name,
            selectedRoles: response.data.userRoles
          }))
          .catch(error => console.log('Error:', error));
    }

    handleChange(event) {
        event.preventDefault();
        if (event.target.name === 'selectedRole' && event.target.value !== null) {
          this.setState({ roleError: '' })
        }
        this.setState({ [event.target.name]: event.target.value });
    }

    handleClick = event => {
        event.preventDefault();
        this.props.history.push('/usermanager');
    }

    submitForm(event) {
        event.preventDefault();
        if (this.state.selectedRoles.length > 0) {
            this.axios.instance.post('/api/edituser', {
            id: this.state.id,
            name: this.state.firstName,
            surname: this.state.surname,
            email: this.state.email,
            password: this.state.password,
            roles: this.state.selectedRoles,
            middlename: this.state.middlename,
          })
          .then(response => this.setState({snackbarMessage:'Изменения сохранены. Вы будете перенаправлены.', 
          snackbarVariant: 'success'}, () => this.refs.child.handleOpen(), setTimeout(() => this.setState({ requestStatus: true }), 2000)))
            .catch(error =>
              this.setState({
                snackbarMesssage: 'Данный email уже используется',
                snackbarVariant: 'error'
              }, () => this.refs.child.handleOpen()))
        }
        else {
          this.setState({ roleError: 'Выберите роль' })
        }
    }

    render() {
        const isAuthentificated = this.authService.isAuthenificated();
        const { classes } = this.props;
        const { firstName, surname, middlename, email, selectedRoles,
            requestStatus, roleError, snackbarMessage, snackbarVariant} = this.state;

        return (
            !isAuthentificated ? <Redirect to="/" /> : (
                requestStatus ? <Redirect to='/usermanager' /> : (
                    <div>
                        <Menu />
                        <CustomSnackBar ref="child" message={snackbarMessage} variant={snackbarVariant} />
                        <Container component="main" maxWidth="xs">
                            <CssBaseline />
                            <div className={classes.papper}>
                                <Avatar className={classes.avatar}>
                                    <Settings />
                                </Avatar>
                                <Typography component="h1" variant="h5">
                                    Добавление пользователя
                                </Typography>
                                <form className={classes.form} onSubmit={this.submitForm.bind(this)}>
                                    <Grid container spacing={1}>
                                        <Grid item xs={12} sm={6}>
                                            <TextField name="firstName"
                                                       variant="outlined"
                                                       value={firstName}
                                                       required
                                                       fullWidth
                                                       id="firstName"
                                                       onChange={this.handleChange.bind(this)}
                                                       label="Имя"
                                                       autoFocus
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField name="surname"
                                                       variant="outlined"
                                                       value={surname}
                                                       required
                                                       fullWidth
                                                       id="surname"
                                                       onChange={this.handleChange.bind(this)}
                                                       label="Фамилия"
                                                       
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField name="middlename"
                                                       variant="outlined"
                                                       value={middlename}
                                                       fullWidth
                                                       id="middlename"
                                                       onChange={this.handleChange.bind(this)}
                                                       label="Отчество"
                                                       
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField name="email"
                                                       variant="outlined"
                                                       value={email}
                                                       required
                                                       fullWidth
                                                       id="email"
                                                       type="email"
                                                       onChange={this.handleChange.bind(this)}
                                                       label="Email(Логин)"
                                                       
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormControl className={classes.formControl} fullWidth required>
                                                <InputLabel htmlFor="select-multiple-checkbox">Роль</InputLabel>
                                                <Select multiple
                                                        value={selectedRoles}
                                                        name="selectedRoles"
                                                        onChange={this.handleChange.bind(this)}
                                                        input={ <Input id="select-multiple-checkbox" /> }
                                                        renderValue={selected => selected.join(', ')}
                                                >
                                                    {this.state.roles.map(role => 
                                                        <MenuItem key={role} value={role}>
                                                            <Checkbox checked={ selectedRoles.indexOf(role) > -1 } />
                                                            <ListItemText primary={role}/>
                                                        </MenuItem>
                                                    )}
                                                </Select>
                                                <FormHelperText error>{roleError}</FormHelperText>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Button fullWidth
                                                    type="submit"
                                                    color="primary"
                                                    variant="contained"
                                                    className={classes.submit}
                                            >
                                                Изменить
                                            </Button>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Button fullWidth
                                                    onClick={this.handleClick}
                                                    variant="contained"
                                                    className={classes.submit}
                                            >
                                                Отменить
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </form>
                            </div>
                        </Container>
                    </div>
                )
            )
        )
    }
}

EditUserPage.propTypes = {
    classes: PropTypes.object.isRequired
};
export default withStyles(EditFormTheme)(EditUserPage);