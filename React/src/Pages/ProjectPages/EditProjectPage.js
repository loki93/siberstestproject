import React from 'react';
import Menu from '../../components/Menu';
import AuthService from '../../services/AuthService';
import axios from '../../services/axios';
import EditFormTheme from '../../styles/CreatePageStyles';
import CustomSnackBar from '../../components/SnackBar';
import { Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Container, CssBaseline, Avatar, Typography, Grid, TextField, FormControl, InputLabel, Select, Input, MenuItem, ListItemText, Button } from '@material-ui/core';
import Settings from '@material-ui/icons/Settings';

class EditProjectPage extends React.Component {

    constructor(props) {
        super(props);
        this.authService = new AuthService();
        this.axios = new axios();
        this.state = {
            id: '',
            name: '',
            customerCompanyName: '',
            executingCompanyName: '',
            supervisorProjectId: '',
            beginDate: '',
            endDate: '',
            priority: '',
            managers: [],
            users: [],
            executingEmployeesId: [],
            selectedExecutingEmployees: [],
            requestStatus: false,
            managerError: '',
            helperStatus: ''
        }
    }

    uri = this.props.location.pathname.split('/');

    componentDidMount() {
        this.axios.instance.get('api/project/' + this.uri[2])
            .then(response => this.setState({
                id: response.data.id,
                name: response.data.name,
                customerCompanyName: response.data.customerCompanyName,
                executingCompanyName: response.data.executingCompanyName,
                supervisorProjectId: response.data.supervisorProjectId,
                beginDate: response.data.beginDate,
                endDate: response.data.endDate,
                priority: response.data.priority,
                executingEmployeesId: response.data.executingEmployeesId,
                users: response.data.users
            }))
            .catch(error => console.log('Error:', error));
        ;
        this.axios.instance.get('/api/usermanager?selectedRole=Manager')
            .then(response => this.setState({ managers: response.data }))
            .catch(err => console.log(err));
    }

    handleChange(event) {
        event.preventDefault();
        if (event.target.name === 'select-manager' && event.target.value !== null) {
            this.setState({ managerError: '' })
        }
        this.setState({ [event.target.name]: event.target.value });
    }

    handleClick = event => {
        event.preventDefault();
        this.props.history.push('/projects');
    }

    submitForm(event) {
        event.preventDefault();
        if (this.state.supervisorProjectId !== null) {
            this.axios.instance.post('/api/editproject', {
                id: this.state.id,
                name: this.state.name,
                customerCompanyName: this.state.customerCompanyName,
                executingCompanyName: this.state.executingCompanyName,
                supervisorProjectId: this.state.supervisorProjectId,
                beginDate: this.state.beginDate,
                endDate: this.state.endDate,
                priority: this.state.priority,
                executingEmployeesId: this.state.executingEmployeesId,
            })
                .then(response => response.data.result === true ? this.setState({
                    snackbarMessage: response.data.message,
                    snackbarVariant: 'success'
                }, () => this.refs.child.handleOpen(), setTimeout(() => this.setState({ requestStatus: true }), 2000)) : null)
        }
        else {
            this.setState({ managerError: 'Выберите менеджера проекта' })
        }
    }

    render() {

        const isAuthentificated = this.authService.isAuthenificated();
        const { classes } = this.props;
        const { name, customerCompanyName, executingCompanyName, supervisorProjectId, priority, executingEmployeesId,
            requestStatus, snackbarMessage, snackbarVariant, users } = this.state;
        return (
            !isAuthentificated ? <Redirect to="/" /> : (
                requestStatus ? <Redirect to='/projects' /> : (
                    <div>
                        <Menu />
                        <CustomSnackBar ref="child" message={snackbarMessage} variant={snackbarVariant} />
                        <Container component="main" maxWidth="xs">
                            <CssBaseline />
                            <div className={classes.papper}>
                                <Avatar className={classes.avatar}>
                                    <Settings />
                                </Avatar>
                                <Typography component="h1" variant="h5">
                                    Редактирование проекта
                                </Typography>
                                <form className={classes.form} onSubmit={this.submitForm.bind(this)}>
                                    <Grid container spacing={1}>
                                        <Grid item xs={12} sm={12}>
                                            <TextField name="name"
                                                variant="outlined"
                                                value={name}
                                                required
                                                fullWidth
                                                id="name"
                                                onChange={this.handleChange.bind(this)}
                                                label="Название проекта"
                                                autoFocus
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <TextField name="customerCompanyName"
                                                variant="outlined"
                                                value={customerCompanyName}
                                                required
                                                fullWidth
                                                id="customerCompanyName"
                                                onChange={this.handleChange.bind(this)}
                                                label="Заказчик"

                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField name="executingCompanyName"
                                                variant="outlined"
                                                value={executingCompanyName}
                                                fullWidth
                                                required
                                                id="executingCompanyName"
                                                onChange={this.handleChange.bind(this)}
                                                label="Исполнитель"

                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormControl fullWidth required>
                                                <InputLabel htmlFor="select-manager">Менеджер проекта</InputLabel>
                                                <Select
                                                    fullWidth
                                                    value={supervisorProjectId}
                                                    name="supervisorProjectId"
                                                    onChange={this.handleChange.bind(this)}
                                                    input={<Input id="select-manager" />}
                                                >
                                                    {this.state.managers.map(manager => (
                                                        <MenuItem key={manager.id} value={manager.id}>
                                                            <ListItemText primary={manager.fullname} />
                                                        </MenuItem>
                                                    ))}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={5}>
                                            <FormControl fullWidth required>
                                                <InputLabel htmlFor="select-priority">Приоритет</InputLabel>
                                                <Select
                                                    fullWidth
                                                    value={priority}
                                                    name="priority"
                                                    onChange={this.handleChange.bind(this)}
                                                    input={<Input id="select-priority" />}
                                                >
                                                    <MenuItem value={1}>1</MenuItem>
                                                    <MenuItem value={2}>2</MenuItem>
                                                    <MenuItem value={3}>3</MenuItem>
                                                    <MenuItem value={4}>4</MenuItem>
                                                    <MenuItem value={5}>5</MenuItem>
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormControl fullWidth required>
                                                <InputLabel htmlFor="select-employees">Исполнители</InputLabel>
                                                <Select
                                                    fullWidth
                                                    multiple
                                                    value={executingEmployeesId}
                                                    name="executingEmployeesId"
                                                    onChange={this.handleChange.bind(this)}
                                                    input={<Input id="select-employees" />}
                                                    renderValue={selected => {
                                                        const selectedNames = []
                                                        users.map(item => {
                                                            selected.map(select => {
                                                                if (item.id === select) {
                                                                    selectedNames.push(item.fullname)
                                                                }
                                                            })
                                                        })
                                                        return selectedNames.join(', ')
                                                    }}
                                                >
                                                    {this.state.users.map(user => (
                                                        <MenuItem key={user.id} value={user.id}>
                                                            <ListItemText primary={user.fullname} />
                                                        </MenuItem>
                                                    ))}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Button fullWidth
                                                type="submit"
                                                color="primary"
                                                variant="contained"
                                                className={classes.submit}
                                            >
                                                Редактировать
                                            </Button>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Button fullWidth
                                                onClick={this.handleClick}
                                                variant="contained"
                                                className={classes.submit}
                                            >
                                                Отменить
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </form>
                            </div>
                        </Container>
                    </div>
                )
            )
        )
    }
}
EditProjectPage.propTypes = {
    classes: PropTypes.object.isRequired
};
export default withStyles(EditFormTheme)(EditProjectPage);