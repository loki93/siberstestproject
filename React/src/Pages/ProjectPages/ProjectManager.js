import React, { Component } from 'react';
import axios from '../../services/axios';
import Menu from '../../components/Menu';
import AuthService from '../../services/AuthService';
import { Container, CssBaseline, Grid, TextField, Tooltip, Button, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import Search from '@material-ui/icons/Search';
import AddIcon from '@material-ui/icons/Add';
import '../../styles/usermanager.css';
import Project from '../../components/Project';

class ProjectManager extends Component {

    constructor(props) {
        super(props);
        this.authService = new AuthService();
        this.axios = new axios();
        this.state = {
            projects: [],
            name: "",
            customerCompanyName: "",
            executingCompanyName: "",
            superVisorProjectName: "",
            isUpdateState: false
        }
    }

    componentDidMount() {
        this.authService.setAuthorizeData(localStorage.getItem('token'), localStorage.getItem('roles'));
        this.axios.instance.get('/api/allprojects?userId=' + localStorage.getItem('id'))
            .then(response => this.setState({ projects: response.data }));
    }

    componentDidUpdate() {
        if (this.state.isUpdateState) {
            this.axios.instance.get('/api/?userId=' + localStorage.getItem('id'))
                .then(response => this.setState({ projects: response.data }))
                .catch(err => console.log(err));

            this.setState({ isUpdateState: false });
        }
    }

    handlerRefresh() {
        this.setState({ isUpdateState: true })
    }

    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
    }

    projectAddHandler() {
        this.props.history.push({
            pathname: '/addproject'
        });
    }

    submitSearch(event) {
        event.preventDefault();
        this.axios.instance.get('/api/allprojects?name='
            + this.state.name
            + '&customerCompanyName=' + this.state.customerCompanyName
            + '&executingCompanyName=' + this.state.executingCompanyName
            + '&supervisorProjectName=' + this.state.superVisorProjectName)
            .then(response => this.setState({ projects: response.data }))
            .catch(err => console.log(err));
    }

    render() {
        const { name, customerCompanyName, executingCompanyName, superVisorProjectName } = this.state;
        return (
            <div>
                <Menu />
                <Container component="main" maxWidth="xl" className="container">
                    <CssBaseline />
                    <form onSubmit={this.submitSearch.bind(this)}>
                        <Grid container spacing={6}>
                            <Grid item xs={12} sm={3}>
                                <TextField name="name"
                                    variant="outlined"
                                    id="name"
                                    label="Поиск по названию проекта"
                                    value={name}
                                    style={{ marginTop: 7 }}
                                    onChange={this.handleChange.bind(this)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <TextField name="customerCompanyName"
                                    variant="outlined"
                                    id="customerCompanyName"
                                    label="Поиск по имени заказчика"
                                    value={customerCompanyName}
                                    style={{ marginTop: 7 }}
                                    onChange={this.handleChange.bind(this)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <TextField name="executingCompanyName"
                                    variant="outlined"
                                    id="executingCompanyName"
                                    label="Поиск по имени исполнителя"
                                    value={executingCompanyName}
                                    style={{ marginTop: 7 }}
                                    onChange={this.handleChange.bind(this)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <TextField name="superVisorProjectName"
                                    variant="outlined"
                                    id="superVisorProjectName"
                                    label="Поиск по имени руководителя"
                                    value={superVisorProjectName}
                                    style={{ marginTop: 7 }}
                                    onChange={this.handleChange.bind(this)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={6} sm={2}>
                                <Tooltip title="Поиск">
                                    <Button type="submit"
                                        variant="contained"
                                        color="primary"
                                        fullWidth
                                        style={{ marginTop: 20 }}
                                    >
                                        <Search />
                                    </Button>
                                </Tooltip>
                            </Grid>
                            <Grid item xs={6} sm={1}>
                                <Tooltip title="Добавить проект">
                                    <Button type="button"
                                        onClick={this.projectAddHandler.bind(this)}
                                        variant="contained"
                                        color="primary"
                                        fullWidth
                                        style={{ marginTop: 20 }}
                                    >
                                        <AddIcon />
                                    </Button>
                                </Tooltip>
                            </Grid>
                        </Grid>
                    </form>
                    <Grid container spacing={4}>
                        <Grid item xs={12} sm={12}>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Название проекта</TableCell>
                                        <TableCell>Заказчик</TableCell>
                                        <TableCell>Исполнитель</TableCell>
                                        <TableCell>Руководитель</TableCell>
                                        <TableCell>Дата начала</TableCell>
                                        <TableCell>Дата конца</TableCell>
                                        <TableCell>Приоритет</TableCell>
                                        <TableCell>Исполнители</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.projects.map(project => (
                                        <Project key={project.id} item={project} handlerRefresh={this.handlerRefresh.bind(this)} />
                                    ))}
                                </TableBody>
                            </Table>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        )
    }
}

export default ProjectManager;