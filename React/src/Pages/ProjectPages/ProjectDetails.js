import React, { Component } from 'react';
import axios from '../../services/axios';
import Menu from '../../components/Menu';
import AuthService from '../../services/AuthService';
import Search from '@material-ui/icons/Search';
import AddIcon from '@material-ui/icons/Add';
import '../../styles/usermanager.css';
import { Container, Grid, TextField, InputLabel, Select, Input, MenuItem, ListItemText, Tooltip, Button, FormControl, Typography, Table, TableHead, TableRow, TableCell, TableBody} from '@material-ui/core';
import Ticket from '../../components/Ticket';
import { withRouter} from 'react-router-dom';

class ProjectDetails extends Component {
    constructor(props) {
        super(props);
        this.authService = new AuthService();
        this.axios = new axios();
        this.state = {
            tickets: [],
            name: "",
            authorName: "",
            executingEmployeeName: "",
            status: "Все статусы",
            projectName: "",
            projectManagerName: "",
            priorityProject: "",
            beginDate: "",
            endDate: "",
            customer: "",
            executingCompanyName: "",
            executingEmployeesName: [],
            executingEmployeesId: [],
            isUpdateState: false
        }
    }

    uri = this.props.location.pathname.split('/');

    componentDidMount() {
        this.authService.setAuthorizeData(localStorage.getItem('token'), localStorage.getItem('roles'));
        this.axios.instance.get('api/project/' + this.uri[2])
            .then(response => this.setState({
                projectName: response.data.name,
                projectManagerName: response.data.supervisorProjectName,
                priorityProject: response.data.priority,
                beginDate: response.data.beginDate,
                endDate: response.data.endDate,
                customer: response.data.customerCompanyName,
                executingCompanyName: response.data.executingCompanyName,
                executingEmployeesName: response.data.executingEmployeesName,
                executingEmployeesId: response.data.executingEmployeesId
            }));
            this.axios.instance.get('/api/alltickets?projectId=' + this.uri[2])
            .then(response => this.setState({
                tickets: response.data,
            }));
    }

    componentDidUpdate() {
        if (this.state.isUpdateState) {
            this.axios.instance.get('api/alltickets?projectId=' + this.uri[2])
                .then(response => this.setState({ tickets: response.data }))
                .catch(err => console.log(err));

            this.setState({ isUpdateState: false });
        }
    }

    handlerRefresh() {
        this.setState({ isUpdateState: true })
    }

    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
    }

    ticketAddHandler() {
        this.props.history.push({
            pathname: '/addticket/projectId=' + this.uri[2],
        });
    }

    submitSearch(event) {
        var status = this.state.status;
        event.preventDefault();
        if(status === 'Все статусы'){
            status = []
        }
        this.axios.instance.get('/api/alltickets?name='
            + this.state.name
            + '&authorName=' + this.state.authorName
            + '&executingEmployeeName=' + this.state.executingEmployeeName
            + '&projectId=' + this.uri[2]
            + '&statusName=' + status)
                .then(response => this.setState({ tickets: response.data }))
                .catch(err => console.log(err));
    }

    render() {
        const { name, authorName, executingEmployeeName, status } = this.state;
        return (
            <div>
                <Menu />
                <Container component="main" maxWidth="xl" className="container">
                    <Typography style={{fontSize: 28}}>
                        {this.state.projectName}
                    </Typography>
                    <Typography style={{fontSize: 20}}>
                        Руководитель проекта: {this.state.projectManagerName}
                    </Typography>
                    <Typography style={{fontSize: 20}}>
                        Приоритет: {this.state.priorityProject}
                    </Typography>
                    <Typography style={{fontSize: 20}}>
                        Даты начала-конца: {this.state.beginDate} - {this.state.endDate}
                    </Typography>
                    <Typography style={{fontSize: 20}}>
                        Заказчик: {this.state.customer}
                    </Typography>
                    <Typography style={{fontSize: 20, marginBottom: 20}}>
                        Исполнитель: {this.state.executingCompanyName}
                    </Typography>
                    <form onSubmit={this.submitSearch.bind(this)}>
                        <Grid container spacing={6}>
                            <Grid item xs={12} sm={3}>
                                <TextField name="name"
                                    variant="outlined"
                                    id="name"
                                    label="Поиск по названию задачи"
                                    value={name}
                                    style={{ marginTop: 7 }}
                                    onChange={this.handleChange.bind(this)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <TextField name="authorName"
                                    variant="outlined"
                                    id="authorName"
                                    label="Поиск по имени автора"
                                    value={authorName}
                                    style={{ marginTop: 7 }}
                                    onChange={this.handleChange.bind(this)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <TextField name="executingEmployeeName"
                                    variant="outlined"
                                    id="executingEmployeeName"
                                    label="Поиск по имени исполнителя"
                                    value={executingEmployeeName}
                                    style={{ marginTop: 7 }}
                                    onChange={this.handleChange.bind(this)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <TextField name="executingEmployeeName"
                                    variant="outlined"
                                    id="executingEmployeeName"
                                    label="Поиск по имени исполнителя"
                                    value={executingEmployeeName}
                                    style={{ marginTop: 7 }}
                                    onChange={this.handleChange.bind(this)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <FormControl fullWidth>
                                    <InputLabel htmlFor="select-status">Фильтр по статусу</InputLabel>
                                    <Select fullWidth
                                        value={status}
                                        name="status"
                                        onChange={this.handleChange.bind(this)}
                                        input={<Input id="select-status" />}
                                    >
                                        <MenuItem value={"Все статусы"}>
                                            <ListItemText primary={'Все статусы'} />
                                        </MenuItem>
                                        <MenuItem value={"Сделать"}>
                                            <ListItemText primary={'Сделать'} />
                                        </MenuItem>
                                        <MenuItem value={"В работе"}>
                                            <ListItemText primary={'В работе'} />
                                        </MenuItem>
                                        <MenuItem value={"Сделано"}>
                                            <ListItemText primary={'Сделано'} />
                                        </MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={6} sm={2}>
                                <Tooltip title="Поиск">
                                    <Button type="submit"
                                        variant="contained"
                                        color="primary"
                                        fullWidth
                                        style={{ marginTop: 20 }}
                                    >
                                        <Search />
                                    </Button>
                                </Tooltip>
                            </Grid>
                            <Grid item xs={6} sm={1}>
                                <Tooltip title="Добавить задачу">
                                    <Button type="button"
                                        onClick={this.ticketAddHandler.bind(this)}
                                        variant="contained"
                                        color="primary"
                                        fullWidth
                                        style={{ marginTop: 20 }}
                                    >
                                        <AddIcon />
                                    </Button>
                                </Tooltip>
                            </Grid>
                        </Grid>
                    </form>
                    <Grid container spacing={4}>
                        <Grid item xs={12} sm={12}>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Название задачи</TableCell>
                                        <TableCell>Автор</TableCell>
                                        <TableCell>Статус</TableCell>
                                        <TableCell>Приоритет</TableCell>
                                        <TableCell>Исполнитель</TableCell>
                                        <TableCell>Комментарий</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {
                                        this.state.tickets.map(ticket => (
                                        <Ticket key={ticket.id} item={ticket} handlerRefresh={this.handlerRefresh.bind(this)}/>
                                    ))}
                                </TableBody>
                            </Table>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        )
    }
}

export default withRouter(ProjectDetails);