import React, { Component } from 'react';
import '../../styles/Home.css';
import AuthService from '../../services/AuthService';
import { Redirect } from 'react-router-dom';
import Menu from '../../components/Menu';
import Footer from '../../components/Footer';
import axios from '../../services/axios';
import ProjectCard from '../../components/ProjectCard';

class Home extends Component {
    constructor(props) {
        super(props);
        this.authService = new AuthService();
        this.authService.setAuthorizeData(localStorage.getItem('token'), localStorage.getItem('roles'));
        this.axios = new axios();
        this.state = {
            projects: []
        }
    }

    componentDidMount() {
        this.axios.instance('/api/allprojects?userId=' + localStorage.getItem('id'))
            .then(response => this.setState({ projects: response.data }));
    }

    render() {
        const isAuthentificated = this.authService.isAuthenificated();
        this.authService.setAuthorizeData(localStorage.getItem('token'), localStorage.getItem('roles'));
        return (
            !isAuthentificated ? <Redirect to="/" /> : (
                <div className="#page-wrap">
                    <Menu />
                    <div className="jumbotron content">
                        <div className="content-data">
                            {this.state.projects.map(project => (
                                <ProjectCard key={project.id} item={project} />
                            ))}
                        </div>
                    </div>
                    <Footer />
                </div>
            )
        )
    }
}

export default Home;