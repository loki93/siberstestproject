import React, { Component } from 'react';
import AuthService from '../../services/AuthService';
import theme from '../../styles/SignInStyles';
import superagent from 'superagent';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import CustomSnackBar from '../../components/SnackBar';
import { Container, CssBaseline, Avatar, Typography, TextField, FormControlLabel, Checkbox, Button } from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { withStyles} from '@material-ui/core/styles';

class SignIn extends Component {
    constructor() {
        super();
        this.authService = new AuthService();
        this.state = {
            email: "",
            password: ""
        }
    }

    handlerEmailChanged(event) {
        this.setState({email: event.target.value});
    }

    handlerPasswordChanged(event) {
        this.setState({ passwordError: ''});
        this.setState({ errorPass: false});
        this.setState({ password: event.target.value});
    }

    submitForm(event) {
        event.preventDefault();
        if (this.authService.passwordValidator(this.state.password) === true) {
            superagent
                .post('https://localhost:44302/login')
                .send({ email: this.state.email, password: this.state.password})
                .end((err, res) => {
                    if(err){
                        if(res === undefined){
                            this.setState({
                                snackbarVariant: "error",
                                snackbarMessage: "Невозможно подключиться к серверу"
                            });
                            this.refs.child.handleOpen();
                            return;
                        }
                        if(res.statusCode === 403) {
                            this.setState({
                                snackbarVariant: "error",
                                snackbarMessage: "Ваша учетная запись заблокирована"
                            });
                            this.refs.child.handleOpen();
                            return;
                        }
                        else {
                            this.setState({
                                snackbarVariant: "error",
                                snackbarMessage: "Неверный логин и/или пароль"
                            });
                            this.refs.child.handleOpen();
                            return;
                        }
                    }
                    else {
                        this.authService.login(res.body.token, res.body.roles);
                        this.authService.setAuthorizeData(res.body.token, res.body.roles);
                        this.authService.setUserId(res.body.id);
                        this.setState({});
                    }
                    
                });
        }
        else {
            this.setState({
                passwordError: 
                    "Пароль должен состоять минимум из 6 символов," +
                    "из которых: 1 строчный, 1 заглавный и 1 цифра",
                errorPass: true
            });
        }
    }

    render() {
        const { classes } = this.props;
        const isAuthentificated = this.authService.isAuthenificated();
        return (
            isAuthentificated ? <Redirect to={{pathname: "/home"}} /> : (
                <Container component="main" maxWidth="xs">
                    <CssBaseline />
                    <CustomSnackBar ref='child' variant={this.state.snackbarVariant} message={this.state.snackbarMessage}/>
                    <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Авторизация
                        </Typography>
                        <form className={classes.form} onSubmit={this.submitForm.bind(this)}>
                            <TextField variant="outlined"
                                       margin="normal"
                                       required
                                       fullWidth
                                       id="email"
                                       label="Email"
                                       name="email"
                                       type="email"
                                       autoComplete="email"
                                       autoFocus
                                       value={this.state.email}
                                       onChange={this.handlerEmailChanged.bind(this)} />
                            <TextField variant="outlined"
                                        margin="normal"
                                        fullWidth
                                        required
                                        value={this.state.password}
                                        type="password"
                                        onChange={this.handlerPasswordChanged.bind(this)}
                                        helperText={this.state.passwordError}
                                        error={this.state.errorPass}
                                        name="password"
                                        label="Пароль"
                                        id="password"
                                        autoComplete="current-password" />
                            <FormControlLabel control={ <Checkbox value="remember" color="primary"/> } 
                                              label="Запомнить меня?" />
                            <Button type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={classes.submit}>
                                        Войти
                            </Button>
                        </form>
                    </div>
                </Container>
            )
        )
    }
}

SignIn.propTypes = {
    classes: PropTypes.object.isRequired
}
export default withStyles(theme)(SignIn);
