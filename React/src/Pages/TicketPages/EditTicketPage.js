import React from 'react';
import axios from '../../services/axios';
import AuthService from '../../services/AuthService';
import { withStyles } from '@material-ui/core/styles';
import Menu from '../../components/Menu';
import CustomSnackBar from '../../components/SnackBar';
import EditFormTheme from '../../styles/CreatePageStyles';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Container, CssBaseline, Avatar, Typography, Grid, TextField, FormControl, InputLabel, Select, Input, MenuItem, ListItemText, Button } from '@material-ui/core';
import Settings from '@material-ui/icons/Settings';

class EditTicketPage extends React.Component {
    constructor(props) {
        super(props);
        this.authService = new AuthService();
        this.axios = new axios();
        this.state = {
            id: '',
            name: '',
            authorId: '',
            authorName: '',
            executingEmployeeId: '',
            executingEmployeeName: '',
            status: '',
            statusName: '',
            commentary: '',
            priority: '',
            projectId: '',
            projectName: '',
            users: [],
            requestStatus: false,
            employeeError: '',
            helperStatus: ''
        }
    }

    uri = this.props.location.pathname.split('/');

    componentDidMount() {
        this.axios.instance.get('/api/ticket/' + this.uri[2])
            .then(response => (
                this.setState({
                    id: response.data.id,
                    name: response.data.name,
                    authorId: response.data.authorId,
                    authorName: response.data.authorName,
                    executingEmployeeId: response.data.executingEmployeeId,
                    executingEmployeeName: response.data.executingEmployeeName,
                    status: response.data.status,
                    statusName: response.data.statusName,
                    commentary: response.data.commentary,
                    priority: response.data.priority,
                    projectId: response.data.projectId,
                    projectName: response.data.projectName
                }),
                this.axios.instance.get('/api/project/' + response.data.projectId)
                    .then(res => this.setState({
                        users: res.data.users
                    }))
            ))
            .catch(err => console.log(err))
    }

    handleChange(event) {
        event.preventDefault();
        if (event.target.name === 'select-employee' && event.target.value !== null) {
            this.setState({ employeeError: '' })
        }
        if(event.target.name === 'status') {
            this.setState({ statusName: event.target.value})
        }
        this.setState({ [event.target.name]: event.target.value });
    }

    handleClick = event => {
        event.preventDefault();
        this.props.history.push('/project/' + this.state.projectId);
    }

    submitForm(event) {
        event.preventDefault();
        if (this.state.executingEmployeeId !== null) {
            this.axios.instance.post('/api/editticket', {
                id: this.state.id,
                Name: this.state.name,
                AuthorId: this.state.authorId,
                ExecutingEmployeeId: this.state.executingEmployeeId,
                Status: this.state.status,
                Commentary: this.state.commentary,
                Priority: this.state.priority,
                ProjectId: this.state.projectId
            })
                .then(response => response.data.result === true ? this.setState({
                    snackbarMessage: response.data.message,
                    snackbarVariant: 'success'
                }, () => this.refs.child.handleOpen(), setTimeout(() => this.setState({ requestStatus: true }), 2000)) : null);
        }
        else {
            this.setState({ employeeError: 'Выберите исполнителя задачи' })
        }
    }

    render() {
        const isAuthentificated = this.authService.isAuthenificated();
        const { classes } = this.props;
        const { requestStatus, snackbarMessage, snackbarVariant, name, executingEmployeeId, commentary, priority, statusName, status } = this.state;
        return (
            !isAuthentificated ? <Redirect to="/" /> : (
                requestStatus ? <Redirect to={`/project/${this.state.projectId}`} /> : (
                    <div>
                        <Menu />
                        <CustomSnackBar ref="child" message={snackbarMessage} variant={snackbarVariant} />
                        <Container component="main" maxWidth="xs">
                            <CssBaseline />
                            <div className={classes.papper}>
                                <Avatar className={classes.avatar}>
                                    <Settings />
                                </Avatar>
                                <Typography component="h1" variant="h5">
                                    Редактирование задачи
                                </Typography>
                                <form className={classes.form} onSubmit={this.submitForm.bind(this)}>
                                    <Grid container spacing={1}>
                                        <Grid item xs={12} sm={12}>
                                            <TextField name="name"
                                                variant="outlined"
                                                value={name}
                                                required
                                                fullWidth
                                                id="name"
                                                onChange={this.handleChange.bind(this)}
                                                label="Название задачи"
                                                autoFocus
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormControl fullWidth required>
                                                <InputLabel htmlFor="select-employee">Исполнитель задачи</InputLabel>
                                                <Select
                                                    fullWidth
                                                    value={executingEmployeeId}
                                                    name="executingEmployeeId"
                                                    onChange={this.handleChange.bind(this)}
                                                    input={<Input id="select-employee" />}
                                                >
                                                    {this.state.users.map(user => (
                                                        <MenuItem key={user.id} value={user.id}>
                                                            <ListItemText primary={user.fullname} />
                                                        </MenuItem>
                                                    ))}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormControl fullWidth required>
                                                <InputLabel htmlFor="select-status">Статус задачи</InputLabel>
                                                <Select
                                                    fullWidth
                                                    value={status}
                                                    name="status"
                                                    onChange={this.handleChange.bind(this)}
                                                    input={<Input id="select-status" />}
                                                    renderValue = {selected => {
                                                        return statusName;
                                                    }}
                                                >
                                                    <MenuItem value={"ToDo"}>
                                                        <ListItemText primary={'Сделать'} />
                                                    </MenuItem>
                                                    <MenuItem value={"InProgress"}>
                                                        <ListItemText primary={'В работе'} />
                                                    </MenuItem>
                                                    <MenuItem value={"Done"}>
                                                        <ListItemText primary={'Сделано'} />
                                                    </MenuItem>
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <TextField name="commentary"
                                                variant="outlined"
                                                value={commentary}
                                                multiline
                                                rows="4"
                                                fullWidth
                                                id="commentary"
                                                onChange={this.handleChange.bind(this)}
                                                label="Комментарий"
                                                autoFocus
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <FormControl fullWidth required>
                                                <InputLabel htmlFor="select-priority">Приоритет</InputLabel>
                                                <Select
                                                    fullWidth
                                                    value={priority}
                                                    name="priority"
                                                    onChange={this.handleChange.bind(this)}
                                                    input={<Input id="select-priority" />}
                                                >
                                                    <MenuItem value={1}>1</MenuItem>
                                                    <MenuItem value={2}>2</MenuItem>
                                                    <MenuItem value={3}>3</MenuItem>
                                                    <MenuItem value={4}>4</MenuItem>
                                                    <MenuItem value={5}>5</MenuItem>
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Button fullWidth
                                                type="submit"
                                                color="primary"
                                                variant="contained"
                                                className={classes.submit}
                                            >
                                                Редактировать
                                            </Button>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Button fullWidth
                                                onClick={this.handleClick}
                                                variant="contained"
                                                className={classes.submit}
                                            >
                                                Отменить
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </form>
                            </div>
                        </Container>
                    </div>
                )
            )
        )
    }
}
EditTicketPage.propTypes = {
    classes: PropTypes.object.isRequired
};
export default withStyles(EditFormTheme)(EditTicketPage);