import React, { Component } from 'react';
import '../styles/User.css';
import { TableRow, TableCell, Tooltip, Button } from '@material-ui/core';
import Create from '@material-ui/icons/Create';
import DeleteModal from './Modal/DeleteModal';
import { withRouter } from 'react-router-dom';

class Ticket extends Component {

    state = {
        id: this.props.item.id,
        action: null
    }

    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
        this.setState({ action: event.target.value });
    }

    actionHandler(event, id, action) {
        event.preventDefault();
        this.props.history.push('/' + action + '/' + id);
    }

    render() {
        const isCreatorTicket = this.props.item.authorId === localStorage.getItem('id') || this.props.item.executingEmployeeId === localStorage.getItem('id');
        return (
            <TableRow key={this.props.item.id}>
                <TableCell>{this.props.item.name}</TableCell>
                <TableCell>{this.props.item.authorName}</TableCell>
                <TableCell>{this.props.item.statusName}</TableCell>
                <TableCell>{this.props.item.priority}</TableCell>
                <TableCell>{this.props.item.executingEmployeeName}</TableCell>
                <TableCell>{this.props.item.commentary}</TableCell>
                {isCreatorTicket ? <div>
                    <TableCell className="buttonPadding"
                    align="right"
                >
                    <Tooltip title="Редактировать">
                        <Button type="button"
                            variant="contained"
                            onClick={event => this.actionHandler(event, this.props.item.id, 'editticket')}
                        >
                            <Create />
                        </Button>
                    </Tooltip>
                </TableCell>
                <TableCell className="buttonPadding"
                        align="left"
                    >
                        <DeleteModal id={this.props.item.id}
                            handlerRefresh={this.props.handlerRefresh}
                            path="api/deleteticket"
                        />

                    </TableCell>
                </div> : null}
            </TableRow>
        )
    }
}

export default withRouter(Ticket);