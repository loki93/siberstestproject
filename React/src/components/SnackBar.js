import React, {Component} from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import WarningIcon from '@material-ui/icons/Warning';
import { makeStyles } from '@material-ui/core/styles';
import SnackBarStyles from  '../styles/SnackBarStyles';

const variantIcon = {
    error: ErrorIcon,
    success: CheckCircleIcon,
    warning: WarningIcon,
    info: InfoIcon
}

const makingStyle = makeStyles(SnackBarStyles);

function MySnackBarContentWrapper(props) {
    const classes = makingStyle();
    const { className, message, onClose, variant, ...other} = props;
    const Icon = variantIcon[variant];
    return (
        <SnackbarContent 
            className={clsx(classes[variant], className)}
            aria-describedby="client-snackbar"
            message={
                <span id="client-snackbar" className={classes.message}>
                    <Icon className={clsx(classes.icon, classes.IconVariant)}/>
                    {message}
                </span>
            }
            action={[
                <IconButton key="close" aria-label="close" color="inherit" onClick={onClose}>
                    <CloseIcon className={classes.icon}/>
                </IconButton>
            ]}
            {...other}
        />
    );
}

MySnackBarContentWrapper.propTypes = {
    className: PropTypes.string,
    message: PropTypes.string,
    onClose: PropTypes.func
}

class CustomSnackBar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false
        }
    }

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        this.setState({open: false})
    }

    handleOpen = () => {
        this.setState({open: true})
    }

    render () {
        return (
            <div>
                <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                          open={this.state.open}
                          autoHideDuration={4000}
                          onClose={this.handleClose}>
                              <MySnackBarContentWrapper onClose={this.handleClose}
                                                        variant={this.props.variant}
                                                        message={this.props.message}/>

                </Snackbar>
            </div>
        )
    }
}
export default CustomSnackBar;