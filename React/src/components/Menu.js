import React, {Component} from 'react';
import AuthService from '../services/AuthService';
import {NavLink} from 'react-router-dom';
import Home from '@material-ui/icons/Home';
import SideBar from './SideBar';

class Menu extends Component {
    constructor(props){
        super(props);
        this.AuthService = new AuthService();
    }

    render() {
        const Roles = this.AuthService.checkForRoles();
        let SupervisorPage;
        let commonLayout;
        if(Roles !== null) {
            SupervisorPage = [
                <div key={'supervisorPage div'}>
                    <SideBar />
                    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mr-auto">
                                <NavLink className="btn btn-primary" style={{marginLeft:75}} to="/home"><Home /></NavLink>
                                <li className="nav-item ml-5">
                                    <h3 className="text-white"> Вы вошли под ролью: {Roles}</h3>
                                </li>
                            </ul>
                            <NavLink className="btn btn-primary float-left" to="/" onClick={() => this.AuthService.logout()}>Выход</NavLink>
                        </div>
                    </nav>
                </div>
            ]
        }
        else {
            let AllPage = [
                <li className="nav-item mr-4">
                    <NavLink to="/home">Главная</NavLink>
                </li>,
                <li className="nav-item mr-3">
                    <h3 className="text-white"> Вы вошли под ролью: {Roles}</h3>
                </li>
            ]
            commonLayout = [
                <div>
                    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mr-auto">
                                {AllPage}
                            </ul>
                            <li className="nav-item list-unstyled">
                                <NavLink className="btn btn-primary float-left" to="/" onClick={() => this.AuthService.logout()}>Выход</NavLink>
                            </li>
                        </div>
                    </nav>
                </div>
            ]
        }
        return (
            <div>
                
                {SupervisorPage}
                {commonLayout}
            </div>
        )
    }
}
export default Menu;