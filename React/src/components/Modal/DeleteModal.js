import React, { Component } from 'react';
import axios from '../../services/axios';
import { Button, Tooltip, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';

class DeleteUserModal extends Component {
    constructor() {
        super();
        this.axios = new axios();
        this.state = {
            open: false
        }
    }

    handleClickedOpen(event) {
        event.preventDefault();
        this.setState({ open: true });
    }

    handleClose(event) {
        event.preventDefault();
        this.setState({ open: false });
    }

    deleteHandler(event) {
        event.preventDefault();
        this.setState({ open : false });
        this.axios.instance
           .post(this.props.path, null, {
               params: {
                   id: this.props.id
               }
           })
           .then(response => {
               this.props.handleRefresh();
           })
           .catch(err => console.warn(err));
    }

    render() {
        return (
            <div>
                <Tooltip title="Удалить">
                    <Button type="button"
                            variant="contained"
                            color="secondary"
                            onClick={this.handleClickedOpen.bind(this)}>
                                <DeleteIcon />
                    </Button>           
                </Tooltip>
                <Dialog open={this.state.open}
                        onClose={this.handleClose.bind(this)}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title" />
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Вы уверены, что хотите удалить?
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose.bind(this)}
                                    color="primary"
                                    autoFocus
                            >
                                Нет
                            </Button>
                            <Button onClick={this.deleteHandler.bind(this)}
                                    color="primary"
                            >
                                Да
                            </Button>
                        </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default DeleteUserModal;