import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import { NavLink, withRouter } from 'react-router-dom';
import AuthService from '../services/AuthService';

class SideBar extends React.Component {

    constructor(props) {
        super(props);
        this.authService = new AuthService();
    }

    render() {
        const roles = this.authService.checkForRoles();
        let superVisorBar;
        let managerBar;
        if (roles !== null && roles.includes('Supervisor')) {
            superVisorBar = [
                <NavLink className="menu-item" to="/usermanager" key="usermanager">
                    Управление сотрудниками
                </NavLink>,
                <NavLink className="menu-item" to="/projects" key="projects">
                    Управление проектами
                </NavLink>
            ]
        }
        if(roles !== null && roles.includes('Manager')){
            managerBar = [
                <NavLink className="menu-item" to="/projects" key="projects">
                    Управление проектами
                </NavLink>
            ]
        }
        return (
            <Menu>
                {superVisorBar}
                {managerBar}
            </Menu>
        )
    }
}
export default withRouter(SideBar);