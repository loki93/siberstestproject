import React, { Component } from 'react';
import '../styles/User.css';
import { TableRow, TableCell, Tooltip, Button } from '@material-ui/core';
import Create from '@material-ui/icons/Create';
import { withRouter } from 'react-router-dom';
import DeleteModal from './Modal/DeleteModal';


class Project extends Component {

    state = {
        id: this.props.item.id,
        action: null
    }

    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
        this.setState({ action: event.target.value });
    }

    actionHandler(event, id, action) {
        event.preventDefault();
        this.props.history.push('/' + action + '/' + id);
    }

    render() {
        return (
            <TableRow key={this.props.item.id}>
                <TableCell>{this.props.item.name}</TableCell>
                <TableCell>{this.props.item.customerCompanyName}</TableCell>
                <TableCell>{this.props.item.executingCompanyName}</TableCell>
                <TableCell>{this.props.item.supervisorProjectName}</TableCell>
                <TableCell>{this.props.item.beginDate}</TableCell>
                <TableCell>{this.props.item.endDate}</TableCell>
                <TableCell>{this.props.item.priority}</TableCell>
                <TableCell>{this.props.item.executingEmployeesName.join(', ')}</TableCell>
                <TableCell className="buttonPadding"
                           align="center"
                >
                    <Tooltip title="Редактировать">
                        <Button type="button"
                                variant="contained"
                                onClick={event => this.actionHandler(event, this.props.item.id, 'editproject')}
                        >
                            <Create />
                        </Button>
                    </Tooltip>
                </TableCell>
                <TableCell className="buttonPadding"
                           align="center"
                >
                    <DeleteModal id={this.props.item.id}
                                     handlerRefresh={this.props.handlerRefresh}
                                     path="api/deleteproject"
                    />

                </TableCell>
            </TableRow>
        )
    }
}

export default withRouter(Project);