import React, { Component } from 'react';
import '../styles/User.css';
import { TableRow, TableCell, Tooltip, Button } from '@material-ui/core';
import Create from '@material-ui/icons/Create';
import DeleteModal from './Modal/DeleteModal';
import { withRouter } from 'react-router-dom';

class User extends Component {

    state = {
        id: this.props.item.id,
        action: null
    }

    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
        this.setState({ action: event.target.value });
    }

    actionHandler(event, id, action) {
        event.preventDefault();
        this.props.history.push('/' + action + '/' + id);
    }

    render() {
        return (
            <TableRow key={this.props.item.id}>
                <TableCell>{this.props.item.fullname}</TableCell>
                <TableCell>{this.props.item.email}</TableCell>
                <TableCell>{this.props.item.rolenames.join(', ')}</TableCell>
                <TableCell className="buttonPadding"
                           align="right"
                >
                    <Tooltip title="Редактировать">
                        <Button type="button"
                                variant="contained"
                                onClick={event => this.actionHandler(event, this.props.item.id, 'edituser')}
                        >
                            <Create />
                        </Button>
                    </Tooltip>
                </TableCell>
                <TableCell className="buttonPadding"
                           align="left"
                >
                    <DeleteModal id={this.props.item.id}
                                     handlerRefresh={this.props.handlerRefresh}
                                     path="api/deleteuser"
                    />

                </TableCell>
            </TableRow>
        ) 
    }
}

export default withRouter(User);