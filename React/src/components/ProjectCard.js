import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import CreateFormTheme from '../styles/ProjectCardStyles';
import { Card, CardContent, Typography, CardActions, Button } from '@material-ui/core';
import '../styles/ProjectCardStyles.css'

class ProjectCard extends Component {

    state = {
        id: this.props.item.id,
        action: null
    }

    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
        this.setState({ action: event.target.value });
    }

    actionHandler(event, id, action) {
        event.preventDefault();
        this.props.history.push('/' + action + '/' + id);
    }

    render() {
        const { classes } = this.props;
        return (
            <Card className="card">
                <CardContent>
                    <Typography className={classes.title} color="textPrimary" gutterBottom>
                        {this.props.item.name}
                    </Typography>
                    <Typography>
                        Руководитель: {this.props.item.supervisorProjectName}
                    </Typography>
                    <CardActions>
                        <Button style={{marginTop: 20}} size="medium" variant="contained" type="button" onClick={event => this.actionHandler(event, this.props.item.id, 'project')}>
                            Подробнее
                        </Button>
                    </CardActions>
                </CardContent>
            </Card>
        )
    }
}

export default withStyles(CreateFormTheme)(withRouter(ProjectCard));