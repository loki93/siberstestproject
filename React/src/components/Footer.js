import React from 'react';
import '../styles/bootstrap.css';

const Footer = () => {
    return (
        <div className="container">
            <h6>2019 Copyright by Sibers</h6>
        </div>
    )
}

export default Footer;