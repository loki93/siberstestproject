﻿using Microsoft.AspNetCore.Identity;
using SibersTestProject.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.Data.DataInitializers
{
    public static class IdentityDataInitializers
    {
        private const string SUPERVISOR = "Supervisor";
        private const string MANAGER = "Manager";
        private const string EMPLOYEE = "Employee";

        public static object IdentiryResult { get; private set; }

        public static void SeedData(UserManager<User> UserManager, RoleManager<IdentityRole> RoleManager)
        {
            SeedRoles(RoleManager);
            SeedUsers(UserManager);
        }

        public static void SeedUsers(UserManager<User> UserManager)
        {
            Dictionary<string, string> superVisor = new Dictionary<string, string>
            {
                {"Name", "Supervisor" },
                {"Email", "supervisor@test" },
                {"Password", "Asd123" },
                {"Role", SUPERVISOR },
                {"Surname", "Supervisor" }
            };

            Dictionary<string, string> manager = new Dictionary<string, string>
            {
                {"Name", "Manager" },
                {"Email", "manager@test" },
                {"Password", "Asd123" },
                {"Role", MANAGER },
                {"Surname", "Manager" }
            };

            Dictionary<string, string> employee = new Dictionary<string, string>
            {
                {"Name", "Employee" },
                {"Email", "employee@test" },
                {"Password", "Asd123" },
                {"Role", EMPLOYEE },
                {"Surname", "Employee" }
            };

            List<Dictionary<string, string>> users = new List<Dictionary<string, string>>
            {
                superVisor,
                manager,
                employee
            };
            AddSeedUsers(users, UserManager);
        }

        private static void AddSeedUsers(List<Dictionary<string, string>> users, UserManager<User> userManager)
        {
            foreach(var element in users)
            {
                if(userManager.FindByNameAsync(element["Name"]).Result == null)
                {
                    User user = new User
                    {
                        UserName = element["Name"],
                        Email = element["Email"],
                        Surname = element["Surname"],
                        Name = element["Name"]
                    };

                    IdentityResult result = userManager.CreateAsync(user, element["Password"]).Result;

                    if (result.Succeeded)
                    {
                        userManager.AddToRoleAsync(user, element["Role"]).Wait();
                    }
                }
            }
        }

        public static void SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            List<string> roles = new List<string>
            {
                SUPERVISOR,
                MANAGER,
                EMPLOYEE
            };

            AddSeedRoles(roles, roleManager);
        }

        private static void AddSeedRoles(List<string> roles, RoleManager<IdentityRole> roleManager)
        {
            foreach(var element in roles)
            {
                if (!roleManager.RoleExistsAsync(element).Result)
                {
                    IdentityRole role = new IdentityRole
                    {
                        Name = element
                    };
                    IdentityResult result = roleManager.CreateAsync(role).Result;
                }
            }
        }
    }
}
