﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace SibersTestProject.Data.Entities
{
    public class User : IdentityUser
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        public string MiddleName { get; set; }

        public bool IsDeleted { get; set; }


        public ICollection<ProjectsUsers> Projects { get; set; }
        public ICollection<Ticket> ExecutingTasks { get; set; }
        public ICollection<Ticket> CreatedTasks { get; set; }
        public ICollection<Project> SupervisingProjects { get; set; }
    }
}
