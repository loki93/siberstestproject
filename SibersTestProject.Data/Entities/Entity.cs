﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.Data.Entities
{
    public class Entity
    {
        public int Id { get; set; }

        public bool IsDeleted { get; set; }
    }
}
