﻿using SibersTestProjects.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.Data.Entities
{
    public class Ticket : Entity
    {
        public string Name { get; set; }

        public string AuthorId { get; set; }
        public User Author { get; set; }

        public string ExecutingEmployeeId { get; set; }
        public User ExecutingEmployee { get; set; }

        public StatusTask Status { get; set; }
        public string Commentary { get; set; }
        public int Priority { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; }
    }
}
