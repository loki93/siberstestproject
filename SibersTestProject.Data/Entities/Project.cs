﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.Data.Entities
{
    public class Project : Entity
    {
        public string Name { get; set; }
        public string CustomerCompanyName { get; set; }
        public string ExecutingCompanyName { get; set; }

        public string SupervisorProjectId { get; set; }
        public User SupervisorProject { get; set; }

        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Priority { get; set; }

        public ICollection<ProjectsUsers> ExecutingEmployees { get; set; }
        public ICollection<Ticket> Tasks { get; set; }
    }
}
