﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.Data.Entities
{
    public class ProjectsUsers : Entity
    {
        public string ExecutingEmployeeId { get; set; }
        public User ExecutingEmployee { get; set; }

        public int? ProjectId { get; set; }
        public Project Project { get; set; }
    }
}
