﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.Data.Context
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext CreateContext();
    }
}
