﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.Data.Context
{
    public class ApplicationDbContextFactory : IApplicationDbContextFactory
    {
        private readonly DbContextOptions Options;

        public ApplicationDbContextFactory(DbContextOptions Options)
        {
            this.Options = Options;
        }

        public ApplicationDbContext CreateContext()
        {
            return new ApplicationDbContext(Options);
        }
    }
}
