﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SibersTestProject.Data.Entities;
using SibersTestProject.Data.EntityConfigurations;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.Data.Context
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectsUsers> ProjectsUsers { get; set; }
        public DbSet<Ticket> Tickets { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new ProjectConfiguration());
            builder.ApplyConfiguration(new ProjectUsersConfiguration());
            builder.ApplyConfiguration(new TicketConfiguration());
        }
    }
}
