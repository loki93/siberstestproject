﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SibersTestProject.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.Data.EntityConfigurations
{
    public class ProjectUsersConfiguration : IEntityTypeConfiguration<ProjectsUsers>
    {
        public void Configure(EntityTypeBuilder<ProjectsUsers> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.ExecutingEmployee).WithMany(x => x.Projects).HasForeignKey(x => x.ExecutingEmployeeId).HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Project).WithMany(x => x.ExecutingEmployees).HasForeignKey(x => x.ProjectId).HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
