﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SibersTestProject.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.Data.EntityConfigurations
{
    public class TicketConfiguration : IEntityTypeConfiguration<Ticket>
    {
        public void Configure(EntityTypeBuilder<Ticket> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name).IsRequired(true);
            builder.Property(x => x.Priority).IsRequired(true);
            builder.Property(x => x.Status).IsRequired(true);

            builder.HasOne(x => x.Author).WithMany(x => x.CreatedTasks).HasForeignKey(x => x.AuthorId).HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.ExecutingEmployee).WithMany(x => x.ExecutingTasks).HasForeignKey(x => x.ExecutingEmployeeId).HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Project).WithMany(x => x.Tasks).HasForeignKey(x => x.ProjectId).HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
