﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SibersTestProject.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.Data.EntityConfigurations
{
    public class ProjectConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).IsRequired(true);
            builder.Property(x => x.ExecutingCompanyName).IsRequired(true);
            builder.Property(x => x.CustomerCompanyName).IsRequired(true);
            builder.Property(x => x.Priority).IsRequired(true);
            builder.Property(x => x.BeginDate).IsRequired(true);
            builder.Property(x => x.EndDate).IsRequired(true);

            builder.HasOne(x => x.SupervisorProject).WithMany(x => x.SupervisingProjects).HasForeignKey(x => x.SupervisorProjectId).HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
