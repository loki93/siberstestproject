﻿using SibersTestProject.Data.Context;
using SibersTestProject.Data.Entities;
using SibersTestProject.Data.Repositories;
using SibersTestProject.Data.Repositories.Project;
using SibersTestProject.Data.Repositories.ProjectUsers;
using SibersTestProject.Data.Repositories.Ticket;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SibersTestProject.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext Context;
        private readonly ConcurrentDictionary<Type, object> Repositories;

        public IProjectRepository Project { get; }
        public IProjectsUsersRepository ProjectsUsers { get; }
        public ITicketRepository Ticket { get; }

        public UnitOfWork(ApplicationDbContext context)
        {
            Context = context;
            Repositories = new ConcurrentDictionary<Type, object>();

            Project = new ProjectRepository(Context);
            ProjectsUsers = new ProjectsUsersRepository(Context);
            Ticket = new TicketRepository(Context);
        }

        public async Task<int> CompleteAsync()
        {
            return await Context.SaveChangesAsync();
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity
        {
            return Repositories.GetOrAdd(typeof(TEntity), new Repository<TEntity>(Context)) as IRepository<TEntity>;
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
