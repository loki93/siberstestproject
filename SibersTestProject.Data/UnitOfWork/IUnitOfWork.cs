﻿using SibersTestProject.Data.Entities;
using SibersTestProject.Data.Repositories;
using SibersTestProject.Data.Repositories.Project;
using SibersTestProject.Data.Repositories.ProjectUsers;
using SibersTestProject.Data.Repositories.Ticket;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SibersTestProject.Data.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IProjectRepository Project { get; }
        IProjectsUsersRepository ProjectsUsers { get; }
        ITicketRepository Ticket { get; }

        Task<int> CompleteAsync();

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;
    }
}
