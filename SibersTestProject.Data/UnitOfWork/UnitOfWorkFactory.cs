﻿using SibersTestProject.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.Data.UnitOfWork
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly IApplicationDbContextFactory ContextFactory;

        public UnitOfWorkFactory(IApplicationDbContextFactory contextFactory)
        {
            ContextFactory = contextFactory;
        }
        public IUnitOfWork MakeUnitOfWork()
        {
            return new UnitOfWork(ContextFactory.CreateContext());
        }
    }
}
