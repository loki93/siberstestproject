﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.Data.UnitOfWork
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork MakeUnitOfWork();
    }
}
