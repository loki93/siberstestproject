﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SibersTestProject.Data.Repositories.Project
{
    public interface IProjectRepository : IRepository<Entities.Project> 
    {
        Task<Entities.Project> GetByIdIncludingAsync(int id);
        Task<List<Entities.Project>> GetAllIncludingAsync();
    }
}
