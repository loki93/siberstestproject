﻿using Microsoft.EntityFrameworkCore;
using SibersTestProject.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SibersTestProject.Data.Repositories.Project
{
    public class ProjectRepository : Repository<Entities.Project>, IProjectRepository
    {
        public ProjectRepository(ApplicationDbContext context): base(context)
        {

        }

        public async Task<Entities.Project> GetByIdIncludingAsync(int id)
        {
            return await Context.Projects
                .Include(x => x.Tasks)
                .Include(x => x.ExecutingEmployees)
                    .ThenInclude(x => x.ExecutingEmployee)
                .Include(x => x.SupervisorProject)
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Entities.Project>> GetAllIncludingAsync()
        {
            return await Context.Projects
                .Include(x => x.Tasks)
                .Include(x => x.ExecutingEmployees)
                    .ThenInclude(x => x.ExecutingEmployee)
                .Include(x => x.SupervisorProject).ToListAsync();
        }
    }
}
