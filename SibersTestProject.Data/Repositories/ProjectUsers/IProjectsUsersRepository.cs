﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SibersTestProject.Data.Repositories.ProjectUsers
{
    public interface IProjectsUsersRepository : IRepository<Entities.ProjectsUsers>
    {
        Task CreateRangeAsync(params Entities.ProjectsUsers[] projectsUsers);
        void RemoveRange(params Entities.ProjectsUsers[] projectsUsers);
        void UpdateRange(params Entities.ProjectsUsers[] projectsUsers);
    }
}
