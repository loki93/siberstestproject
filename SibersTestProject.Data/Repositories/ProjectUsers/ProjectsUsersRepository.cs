﻿using SibersTestProject.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SibersTestProject.Data.Repositories.ProjectUsers
{
    public class ProjectsUsersRepository : Repository<Entities.ProjectsUsers>, IProjectsUsersRepository
    {
        public ProjectsUsersRepository(ApplicationDbContext context) : base(context)
        {

        }

        public async Task CreateRangeAsync(params Entities.ProjectsUsers[] projectsUsers)
        {
            await Context.AddRangeAsync(projectsUsers);
        }

        public void RemoveRange(params Entities.ProjectsUsers[] projectsUsers)
        {
            Context.RemoveRange(projectsUsers);
        }

        public void UpdateRange(params Entities.ProjectsUsers[] projectsUsers)
        {
            Context.UpdateRange(projectsUsers);
        }
    }
}
