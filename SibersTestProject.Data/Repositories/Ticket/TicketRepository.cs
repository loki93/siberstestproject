﻿using Microsoft.EntityFrameworkCore;
using SibersTestProject.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SibersTestProject.Data.Repositories.Ticket
{
    public class TicketRepository : Repository<Entities.Ticket>, ITicketRepository
    {
        public TicketRepository(ApplicationDbContext context) : base(context)
        {

        }

        public async Task<List<Entities.Ticket>> GetAllIncludingAsync()
        {
            return await Context.Tickets
                .Include(x => x.Author)
                .Include(x => x.ExecutingEmployee)
                .Include(x => x.Project)
                .ToListAsync();
        }

        public async Task<Entities.Ticket> GetByIdIncludingAsync(int id)
        {
            return await Context.Tickets
                .Include(x => x.Author)
                .Include(x => x.ExecutingEmployee)
                .Include(x => x.Project)
                .SingleOrDefaultAsync(x => x.Id == id);
        }
    }
}
