﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SibersTestProject.Data.Repositories.Ticket
{
    public interface ITicketRepository : IRepository<Entities.Ticket>
    {
        Task<List<Entities.Ticket>> GetAllIncludingAsync();
        Task<Entities.Ticket> GetByIdIncludingAsync(int id);
    }
}
