﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SibersTestProject.BussinessLogic.Services;
using SibersTestProject.Data.Entities;
using SibersTestProjects.Common.Dtos.UserDtos;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace SibersTestProject.Controllers
{
    public class AuthController : Controller
    {
        private readonly UserService service;
        private readonly SignInManager<User> SignInManager;

        public AuthController(UserService service, SignInManager<User> signInManager)
        {
            this.service = service;
            SignInManager = signInManager;
        }

        [Route("login")]
        [HttpPost]
        public async Task<ActionResult> Login([FromBody] LoginDto model)
        {
            if (ModelState.IsValid)
            {
                var user = await service.FindByEmailAsync(model.Email);
                if(user != null && await service.CheckPasswordAsync(user, model.Password) && !user.IsDeleted)
                {
                    var token = await service.GetToken(user);
                    var roles = await service.GetRolesAsync(user);
                    
                    return Ok(new AuthorizeUserDto() { Id = user.Id, Token = new JwtSecurityTokenHandler().WriteToken(token), Roles = await service.GetRolesAsync(user) });
                }
                return StatusCode(403);
            }
            return BadRequest("Invalid login model!");
        }
    }
}
