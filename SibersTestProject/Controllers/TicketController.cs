﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SibersTestProject.BussinessLogic.Services;
using SibersTestProjects.Common.Dtos.TicketDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SibersTestProject.Controllers
{
    [Authorize(Roles = "Supervisor, Manager, Employee")]
    public class TicketController : Controller
    {
        private readonly TicketService service;

        public TicketController(TicketService service)
        {
            this.service = service;
        }

        [Route("api/createticket")]
        [HttpPost]
        public async Task<IActionResult> CreateTicket([FromBody]CreateTicketDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await service.CreateTicketAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/editticket")]
        [HttpPost]
        public async Task<IActionResult> EditTicket([FromBody]CreateTicketDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await service.EditTicketAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/deleteticket")]
        [HttpPost]
        public async Task<IActionResult> DeleteTicket(int id)
        {
            if(id > 0)
            {
                var result = await service.DeleteTicketAsync(id);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/alltickets")]
        [HttpGet]
        public async Task<IActionResult> GetAllTickets(TicketFilterDto filter)
        {
            return Ok(await service.GetAllTicketAsync(filter));
        }

        [Route("api/ticket/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetTicketById(int id)
        {
            if(id > 0)
            {
                return Ok(await service.GetTicketByIdAsync(id));
            }
            return StatusCode(500);
        }
    }
}
