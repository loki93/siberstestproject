﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SibersTestProject.BussinessLogic.Services;
using SibersTestProjects.Common.Dtos.UserDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SibersTestProject.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class UserController : Controller
    {
        private readonly UserService service;

        public UserController(UserService service)
        {
            this.service = service;
        }

        [Route("api/user/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetUserAsync(string id)
        {
            if(id != null)
            {
                return Ok(await service.FindUserByIdAsync(id));
            }
            return StatusCode(500);
        }

        [Route("api/roles")]
        [HttpGet]
        public IActionResult GetAllRoles()
        {
            return Ok(service.GetAllRoles());
        }

        [Route("api/adduser")]
        [HttpPost]
        public async Task<IActionResult> AddUser([FromBody] RegisterDto model)
        {
            if (ModelState.IsValid)
            {
                var foundUser = await service.FindByEmailAsync(model.Email);
                if(foundUser == null)
                {
                    var user = await service.CreateAsync(model, model.Password);
                    if(user != null)
                    {
                        foreach(var role in model.Roles)
                        {
                            await service.AddToRoleAsync(user, role);
                        }
                        return Ok(new { Username = user.UserName });
                    }
                    return StatusCode(500);
                }
                return StatusCode(500);
            }
            return StatusCode(500);
        }

        [Route("api/usermanager")]
        [HttpGet]
        public async Task<IActionResult> GetAllUsers(UserFilterDto filter)
        {
            return Ok(await service.GetAllUsersAsync(filter));
        }

        [Route("api/deleteuser")]
        [HttpPost]
        public async Task<IActionResult> DeleteUser(string id)

        {
            if (!string.IsNullOrEmpty(id))
            {
                var result = await service.DeleteUserAsync(id);
                if (result)
                {
                    return Ok();
                }
            }
            return BadRequest();
        }

        [Route("api/edituser")]
        [HttpPost]
        public async Task<IActionResult> EditUser([FromBody] UserEditDto model)
        {
            if (ModelState.IsValid)
            {
                var user = await service.FindByEmailAsync(model.Email);
                IList<string> roles = null;
                if(user != null)
                {
                    roles = await service.GetRolesAsync(user);
                }
                if(user != null || user.Email != model.Email ||
                    user.Name != model.Name || user.Surname != model.Surname ||
                    user.MiddleName != model.Middlename || roles != model.Roles)
                {
                    var result = await service.EditUserAsync(model);
                    return result == true ? StatusCode(200) : StatusCode(400);
                }
                return StatusCode(400);
            }
            return StatusCode(500);
        }
    }
}
