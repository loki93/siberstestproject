﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SibersTestProject.BussinessLogic.Services;
using SibersTestProjects.Common.Dtos.ProjectDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SibersTestProject.Controllers
{
    [Authorize(Roles = "Supervisor, Manager")]
    public class ProjectController : Controller
    {
        private readonly ProjectService service;

        public ProjectController(ProjectService service)
        {
            this.service = service;
        }

        [Authorize(Roles = "Supervisor")]
        [Route("api/createproject")]
        [HttpPost]
        public async Task<IActionResult> CreateProject([FromBody] ProjectDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await service.CreateProjectAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/editproject")]
        [HttpPost]
        public async Task<IActionResult> EditProject([FromBody] ProjectDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await service.EditProjectAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/deleteproject")]
        [HttpPost]
        public async Task<IActionResult> DeleteProject(int id)
        {
            if(id >= 0)
            {
                var result = await service.DeleteProjectAsync(id);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/allprojects")]
        [HttpGet]
        public async Task<IActionResult> GetAllProjects(ProjectFilterDto filter)
        {
            return Ok(await service.GetAllProjects(filter));
        }

        [Route("api/project/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetProject(int id)
       {
            if(id >= 0)
            {
                return Ok(await service.GetProjectById(id));
            }
            return StatusCode(500);
        }
    }
}
