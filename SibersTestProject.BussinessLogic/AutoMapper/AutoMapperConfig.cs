﻿using AutoMapper;
using SibersTestProject.BussinessLogic.AutoMapper.ProjectMapper;
using SibersTestProject.BussinessLogic.AutoMapper.TicketMapper;
using SibersTestProject.BussinessLogic.AutoMapper.UserMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.BussinessLogic.AutoMapper
{
    public class AutoMapperConfig
    {
        public MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DomainToUserDtoMappingProfile());
                cfg.AddProfile(new UserDtoToDomainMappingProfile());
                cfg.AddProfile(new ProjectDtoToDomainMappingProfile());
                cfg.AddProfile(new DomainToProjectDtoMappingProfile());
                cfg.AddProfile(new TicketDtoToDomainMappingProfile());
                cfg.AddProfile(new DomainToTicketDtoMappingProfile());
            });
        }
    }
}
