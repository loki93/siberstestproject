﻿using AutoMapper;
using SibersTestProject.Data.Entities;
using SibersTestProjects.Common.Dtos.ProjectDtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.BussinessLogic.AutoMapper.ProjectMapper
{
    public class ProjectDtoToDomainMappingProfile : Profile
    {
        public ProjectDtoToDomainMappingProfile()
        {
            CreateProjectDtoMap();
        }

        public void CreateProjectDtoMap()
        {
            CreateMap<ProjectDto, Project>()
                .ForMember(src => src.Id, opts => opts.MapFrom(vm => vm.Id))
                .ForMember(src => src.Name, opts => opts.MapFrom(vm => vm.Name))
                .ForMember(src => src.Priority, opts => opts.MapFrom(vm => vm.Priority))
                .ForMember(src => src.BeginDate, opts => opts.MapFrom(vm => vm.BeginDate))
                .ForMember(src => src.EndDate, opts => opts.MapFrom(vm => vm.EndDate))
                .ForMember(src => src.CustomerCompanyName, opts => opts.MapFrom(vm => vm.CustomerCompanyName))
                .ForMember(src => src.ExecutingCompanyName, opts => opts.MapFrom(vm => vm.ExecutingCompanyName))
                .ForMember(src => src.ExecutingEmployees, opts => opts.Ignore());
        }
    }
}
