﻿using AutoMapper;
using SibersTestProject.Data.Entities;
using SibersTestProjects.Common.Dtos.ProjectDtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.BussinessLogic.AutoMapper.ProjectMapper
{
    public class DomainToProjectDtoMappingProfile : Profile
    {
        public DomainToProjectDtoMappingProfile()
        {
            CreateProjectMap();
        }

        public void CreateProjectMap()
        {
            CreateMap<Project, ProjectDto>()
                .ForMember(x => x.Id, opts => opts.MapFrom(vm => vm.Id))
                .ForMember(x => x.Name, opts => opts.MapFrom(vm => vm.Name))
                .ForMember(x => x.ExecutingCompanyName, opts => opts.MapFrom(vm => vm.ExecutingCompanyName))
                .ForMember(x => x.CustomerCompanyName, opts => opts.MapFrom(vm => vm.CustomerCompanyName))
                .ForMember(x => x.Priority, opts => opts.MapFrom(vm => vm.Priority))
                .ForMember(x => x.SupervisorProjectName, opts => opts.MapFrom(vm => vm.SupervisorProject.Surname + " " + vm.SupervisorProject.Name))
                .ForMember(x => x.ExecutingEmployeesName, opts => opts.Ignore());
        }
    }
}
