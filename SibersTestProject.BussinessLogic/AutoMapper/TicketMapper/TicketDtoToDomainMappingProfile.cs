﻿using AutoMapper;
using SibersTestProject.Data.Entities;
using SibersTestProjects.Common.Dtos.TicketDtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.BussinessLogic.AutoMapper.TicketMapper
{
    public class TicketDtoToDomainMappingProfile : Profile
    {
        public TicketDtoToDomainMappingProfile()
        {
            CreateTicketDtoMap();
        }

        public void CreateTicketDtoMap()
        {
            CreateMap<CreateTicketDto, Ticket>()
                .ForMember(src => src.Id, opts => opts.MapFrom(vm => vm.Id))
                .ForMember(src => src.Name, opts => opts.MapFrom(vm => vm.Name))
                .ForMember(src => src.AuthorId, opts => opts.MapFrom(vm => vm.AuthorId))
                .ForMember(src => src.ExecutingEmployeeId, opts => opts.MapFrom(vm => vm.ExecutingEmployeeId))
                .ForMember(src => src.Commentary, opts => opts.MapFrom(vm => vm.Commentary))
                .ForMember(src => src.Priority, opts => opts.MapFrom(vm => vm.Priority))
                .ForMember(src => src.ProjectId, opts => opts.MapFrom(vm => vm.ProjectId));
        }
    }
}
