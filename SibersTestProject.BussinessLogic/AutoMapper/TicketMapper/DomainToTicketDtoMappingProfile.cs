﻿using AutoMapper;
using SibersTestProject.Data.Entities;
using SibersTestProjects.Common.Dtos.TicketDtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.BussinessLogic.AutoMapper.TicketMapper
{
    public class DomainToTicketDtoMappingProfile : Profile
    {
        public DomainToTicketDtoMappingProfile()
        {
            CreateTicketMap();
        }

        public void CreateTicketMap()
        {
            CreateMap<Ticket, TicketDto>()
                .ForMember(src => src.Id, opts => opts.MapFrom(vm => vm.Id))
                .ForMember(src => src.Name, opts => opts.MapFrom(vm => vm.Name))
                .ForMember(src => src.AuthorId, opts => opts.MapFrom(vm => vm.AuthorId))
                .ForMember(src => src.AuthorName, opts => opts.MapFrom(vm => vm.Author.Surname + " " + vm.Author.Name + " " + vm.Author.MiddleName))
                .ForMember(src => src.ExecutingEmployeeId, opts => opts.MapFrom(vm => vm.ExecutingEmployeeId))
                .ForMember(src => src.ExecutingEmployeeName, opts => opts.MapFrom(vm => vm.ExecutingEmployee.Surname + " " + vm.ExecutingEmployee.Name + " " + vm.ExecutingEmployee.MiddleName))
                .ForMember(src => src.Status, opts => opts.MapFrom(vm => vm.Status))
                .ForMember(src => src.StatusName, opts => opts.Ignore())
                .ForMember(src => src.Commentary, opts => opts.MapFrom(vm => vm.Commentary))
                .ForMember(src => src.Priority, opts => opts.MapFrom(vm => vm.Priority))
                .ForMember(src => src.ProjectId, opts => opts.MapFrom(vm => vm.ProjectId))
                .ForMember(src => src.ProjectName, opts => opts.MapFrom(vm => vm.Project.Name));
        }
    }
}
