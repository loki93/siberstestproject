﻿using AutoMapper;
using SibersTestProject.Data.Entities;
using SibersTestProjects.Common.Dtos.UserDtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.BussinessLogic.AutoMapper.UserMapper
{
    public class UserDtoToDomainMappingProfile : Profile
    {
        public UserDtoToDomainMappingProfile()
        {
            CreateRegisterDtoMap();
            CreateEdiDtoMap();
        }

        public void CreateRegisterDtoMap()
        {
            CreateMap<RegisterDto, User>()
                .ForMember(src => src.Name, opts => opts.MapFrom(vm => vm.Name))
                .ForMember(src => src.Surname, opts => opts.MapFrom(vm => vm.Surname))
                .ForMember(src => src.MiddleName, opts => opts.MapFrom(vm => vm.MiddleName))
                .ForMember(src => src.Email, opts => opts.MapFrom(vm => vm.Email))
                .ForMember(src => src.SecurityStamp, opts => opts.MapFrom(vm => Guid.NewGuid().ToString()))
                .ForMember(src => src.UserName, opts => opts.MapFrom(vm => vm.Email));
        }

        public void CreateEdiDtoMap()
        {
            CreateMap<UserEditDto, User>()
                .ForMember(src => src.Name, opts => opts.MapFrom(vm => vm.Name))
                .ForMember(src => src.Surname, opts => opts.MapFrom(vm => vm.Surname))
                .ForMember(src => src.MiddleName, opts => opts.MapFrom(vm => vm.Middlename))
                .ForMember(src => src.Email, opts => opts.MapFrom(vm => vm.Email))
                .ForMember(src => src.UserName, opts => opts.MapFrom(vm => vm.Email));
        }
    }
}
