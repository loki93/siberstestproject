﻿using AutoMapper;
using SibersTestProject.Data.Entities;
using SibersTestProjects.Common.Dtos.UserDtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace SibersTestProject.BussinessLogic.AutoMapper.UserMapper
{
    public class DomainToUserDtoMappingProfile : Profile
    {
        public DomainToUserDtoMappingProfile()
        {
            CreateUserMap();
            CreateUserEditMap();
        }

        public void CreateUserMap()
        {
            CreateMap<User, UserManagerDto>()
                .ForMember(src => src.Id, opts => opts.MapFrom(vm => vm.Id))
                .ForMember(src => src.Email, opts => opts.MapFrom(vm => vm.Email))
                .ForMember(src => src.Fullname, opts => opts.MapFrom(vm => vm.Surname + " " + vm.Name + " " + vm.MiddleName));
        }

        public void CreateUserEditMap()
        {
            CreateMap<User, UserEditDto>()
                .ForMember(src => src.Id, opts => opts.MapFrom(vm => vm.Id))
                .ForMember(src => src.Middlename, opts => opts.MapFrom(vm => vm.MiddleName))
                .ForMember(src => src.Name, opts => opts.MapFrom(vm => vm.Name))
                .ForMember(src => src.Surname, opts => opts.MapFrom(vm => vm.Surname))
                .ForMember(src => src.Email, opts => opts.MapFrom(vm => vm.Email))
                .ForMember(src => src.Password, opts => opts.MapFrom(vm => vm.PasswordHash));
        }
    }
}
