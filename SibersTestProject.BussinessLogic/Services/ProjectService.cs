﻿using AutoMapper;
using SibersTestProject.Data.Entities;
using SibersTestProject.Data.UnitOfWork;
using SibersTestProjects.Common;
using SibersTestProjects.Common.Dtos.ProjectDtos;
using SibersTestProjects.Common.Dtos.TicketDtos;
using SibersTestProjects.Common.Dtos.UserDtos;
using SibersTestProjects.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SibersTestProject.BussinessLogic.Services
{
    public class ProjectService
    {
        private readonly IUnitOfWorkFactory unitOfWorkFactory;
        private readonly IMapper mapper;
        private readonly UserService userService;

        public ProjectService(IUnitOfWorkFactory unitOfWork, IMapper mapper, UserService userService)
        {
            this.unitOfWorkFactory = unitOfWork;
            this.mapper = mapper;
            this.userService = userService;
        }

        public async Task<OperationResult> CreateProjectAsync(ProjectDto model)
        {
            using(var unitOfWork = unitOfWorkFactory.MakeUnitOfWork())
            {
                var project = mapper.Map<ProjectDto, Project>(model);
                await unitOfWork.Project.CreateAsync(project);
                await unitOfWork.CompleteAsync();

                ProjectsUsers[] projectsUsers = new ProjectsUsers[model.ExecutingEmployeesId.Count];
                for(int i = 0; i < model.ExecutingEmployeesId.Count; i++)
                {
                    projectsUsers[i] = new ProjectsUsers
                    {
                        ExecutingEmployeeId = model.ExecutingEmployeesId[i],
                        ProjectId = project.Id
                    };
                }
                await unitOfWork.ProjectsUsers.CreateRangeAsync(projectsUsers);
                await unitOfWork.CompleteAsync();

                return new OperationResult(true, "Проект успешно создан");
            }
        }

        public async Task<OperationResult> EditProjectAsync(ProjectDto model)
        {
            using(var unitOfWork = unitOfWorkFactory.MakeUnitOfWork())
            {
                var projectsUsers = await unitOfWork.ProjectsUsers.GetAllAsync();
                unitOfWork.ProjectsUsers.RemoveRange(projectsUsers.Where(x => x.ProjectId == model.Id).ToArray());
                await unitOfWork.CompleteAsync();
                ProjectsUsers[] projects = new ProjectsUsers[model.ExecutingEmployeesId.Count];
                for(int i = 0; i < model.ExecutingEmployeesId.Count; i++)
                {
                    projects[i] = new ProjectsUsers
                    {
                        ProjectId = model.Id,
                        ExecutingEmployeeId = model.ExecutingEmployeesId[i]
                    };
                }
                await unitOfWork.ProjectsUsers.CreateRangeAsync(projects);
                var project =  await unitOfWork.Project.GetByIdAsync(model.Id);
                project = mapper.Map(model, project);
                unitOfWork.Project.Update(project);
                await unitOfWork.CompleteAsync();
                return new OperationResult(true, "Проект успешно изменен");
            }
        }

        public async Task<OperationResult> DeleteProjectAsync(int id)
        {
            using(var unitOfWork = unitOfWorkFactory.MakeUnitOfWork())
            {
                var project = await unitOfWork.Project.GetByIdIncludingAsync(id);
                if(project.Tasks.Where(x => x.Status == StatusTask.Done).ToList().Count == 0 && project.Tasks != null)
                {
                    project.IsDeleted = true;
                    unitOfWork.Project.Update(project);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Проект удален!");
                }
                return new OperationResult(false, "Проект удалить невозможно, по нему есть незакрытые задачи");
            }
        }

        public async Task<List<ProjectDto>> GetAllProjects(ProjectFilterDto filter)
        {
            using(var unitOfWork = unitOfWorkFactory.MakeUnitOfWork())
            {
                var projects = await unitOfWork.Project.GetAllIncludingAsync();
                var projectsViewModels = projects.Select(mapper.Map<ProjectDto>).GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).ToList();
                UserEditDto user = new UserEditDto();
                if(filter.UserId != null)
                {
                    user = await userService.FindUserByIdAsync(filter.UserId);
                }
                for(int i = 0; i < projectsViewModels.Count; i++)
                {
                    projectsViewModels[i].ExecutingEmployeesName = new List<string>();
                    projectsViewModels[i].ExecutingEmployeesId = new List<string>();
                    foreach (var name in projects[i].ExecutingEmployees)
                    {
                        projectsViewModels[i].ExecutingEmployeesName.Add(name.ExecutingEmployee.Surname + " " + name.ExecutingEmployee.Name + " " + name.ExecutingEmployee.MiddleName);
                        projectsViewModels[i].ExecutingEmployeesId.Add(name.ExecutingEmployeeId);
                    }
                }

                if(filter.Name != null)
                {
                    projectsViewModels = projectsViewModels.Where(x => x.Name.ToLower().Contains(filter.Name)).ToList();
                }
                
                if(filter.CustomerCompanyName != null)
                {
                    projectsViewModels = projectsViewModels.Where(x => x.CustomerCompanyName.ToLower().Contains(filter.CustomerCompanyName)).ToList();
                }
                if(filter.ExecutingCompanyName != null)
                {
                    projectsViewModels = projectsViewModels.Where(x => x.ExecutingCompanyName.ToLower().Contains(filter.ExecutingCompanyName)).ToList();
                }
                if(filter.SupervisorProjectName != null)
                {
                    projectsViewModels = projectsViewModels.Where(x => x.SupervisorProjectName.ToLower().Contains(filter.SupervisorProjectName)).ToList();
                }
                if(filter.BeginDate != null && filter.EndDate != null)
                {
                    projectsViewModels = projectsViewModels.Where(x => x.BeginDate >= filter.BeginDate && x.EndDate <= filter.EndDate).ToList();
                }
                if(filter.UserId != null && !user.UserRoles.Contains("Supervisor"))
                {
                    projectsViewModels = projectsViewModels.Where(x => x.SupervisorProjectId == filter.UserId || x.ExecutingEmployeesId.Contains(filter.UserId)).ToList();
                }
                return projectsViewModels.Where(x => x.IsDeleted == false).ToList();
            }
        }

        public async Task<List<UserManagerDto>> GetAllUsers()
        {
            UserFilterDto filter = new UserFilterDto
            {
                SelectedRole = "Manager"
            };
            return await userService.GetAllUsersAsync(filter);
        }

        public async Task<ProjectDto> GetProjectById(int id)
        {
            using(var unitOfWork = unitOfWorkFactory.MakeUnitOfWork())
            {
                var project = await unitOfWork.Project.GetByIdIncludingAsync(id);
                var viewModel = mapper.Map<ProjectDto>(project);
                viewModel.Tickets = project.Tasks.Where(x => x.ProjectId == id).Select(mapper.Map<TicketDto>).GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).ToList();
                viewModel.Users = new List<UserManagerDto>();
                viewModel.ExecutingEmployeesId = new List<string>();
                foreach(var user in project.ExecutingEmployees)
                {
                    viewModel.Users.Add(mapper.Map<UserManagerDto>(user.ExecutingEmployee));
                    viewModel.ExecutingEmployeesId.Add(user.ExecutingEmployeeId);
                }
                return viewModel;
            }
        }
    }
}
