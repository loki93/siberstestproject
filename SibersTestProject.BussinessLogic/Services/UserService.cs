﻿using SibersTestProject.Data.Entities;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using AutoMapper;
using System.Threading.Tasks;
using SibersTestProjects.Common.Dtos.UserDtos;
using System.Linq;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;

namespace SibersTestProject.BussinessLogic.Services
{
    public class UserService
    {
        private readonly UserManager<User> UserManager;
        private readonly RoleManager<IdentityRole> RoleManager;
        private readonly IConfiguration Configuration;
        private readonly IMapper Mapper;

        public UserService(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration, IMapper mapper)
        {
            UserManager = userManager;
            RoleManager = roleManager;
            Configuration = configuration;
            Mapper = mapper;
        }

        public async Task<User> CreateAsync(RegisterDto model, string password)
        {
            var user = Mapper.Map<RegisterDto, User>(model);
            await UserManager.CreateAsync(user, password);
            return user;
        }

        public async Task<UserEditDto> FindUserByIdAsync(string id)
        {
            var user = await UserManager.FindByIdAsync(id);
            var editDto = Mapper.Map<User, UserEditDto>(user);
            editDto.UserRoles = await UserManager.GetRolesAsync(user);
            editDto.Roles = GetAllRoles();
            return editDto;
        }

        public List<string> GetAllRoles()
        {
            return RoleManager.Roles.Select(x => x.Name).ToList();
        }

        public async Task AddToRoleAsync(User user, string role)
        {
            await UserManager.AddToRoleAsync(user, role);
        }

        public async Task<IList<string>> GetRolesAsync(User user)
        {
            return await UserManager.GetRolesAsync(user);
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            return await UserManager.FindByEmailAsync(email);
        }

        public async Task<bool> CheckPasswordAsync(User user, string password)
        {
            return await UserManager.CheckPasswordAsync(user, password);
        }

        public async Task<List<UserManagerDto>> GetAllUsersAsync(UserFilterDto filter)
        {
            var users = UserManager.Users.ToList();
            var userViewmodels = UserManager.Users.Select(Mapper.Map<UserManagerDto>).ToList();
            for(int i = 0; i < userViewmodels.Count; i++)
            {
                userViewmodels[i].Rolenames = await UserManager.GetRolesAsync(users[i]);
            }
            if(filter != null)
            {
                if(filter.Fullname != null)
                {
                    userViewmodels = userViewmodels.Where(x => x.Fullname.ToLower().Contains(filter.Fullname.ToLower())).ToList();
                }

                if(filter.SelectedRole != null)
                {
                    userViewmodels = userViewmodels.Where(x => x.Rolenames.Contains(filter.SelectedRole)).ToList();
                }
            }
            return userViewmodels;
        }

        public async Task<bool> EditUserAsync(UserEditDto model)
        {
            var user = await UserManager.FindByIdAsync(model.Id);
            if(user != null)
            {
                var roles = await UserManager.GetRolesAsync(user);
                await UserManager.RemoveFromRolesAsync(user, roles);
                await UserManager.AddToRolesAsync(user, model.Roles);
                user = Mapper.Map(model, user);
                var result = await UserManager.UpdateAsync(user);

                if (result.Succeeded)
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        public async Task<bool> DeleteUserAsync(string id)
        {
            var user = await UserManager.FindByIdAsync(id);
            if(user != null)
            {
                user.IsDeleted = true;
                await UserManager.UpdateAsync(user);

                return true;
            }

            return false;
        }

        public async Task<JwtSecurityToken> GetToken(User user)
        {
            var claims = await GetValidClaims(user);

            var signinKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(Configuration["Jwt:SigningKey"]));

            int expiryInMinutes = Convert.ToInt32(Configuration["Jwt:ExpiryInMinutes"]);

            var token = new JwtSecurityToken(
                issuer: Configuration["Jwt:Site"],
                audience: Configuration["Jwt:Site"],
                expires: DateTime.UtcNow.AddMinutes(expiryInMinutes),
                signingCredentials: new SigningCredentials(signinKey, SecurityAlgorithms.HmacSha256),
                claims: claims
            );
            return token;

        }

        public async Task<List<Claim>> GetValidClaims(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName)
            };
            var userRoles = await UserManager.GetRolesAsync(user);
            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
            }
            return claims;
        }
    }
}
