﻿using AutoMapper;
using SibersTestProject.Data.Entities;
using SibersTestProject.Data.UnitOfWork;
using SibersTestProjects.Common;
using SibersTestProjects.Common.Dtos.TicketDtos;
using SibersTestProjects.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SibersTestProject.BussinessLogic.Services
{
    public class TicketService
    {
        private readonly IUnitOfWorkFactory unitOfWorkFactory;
        private IMapper mapper;

        public TicketService(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
            this.mapper = mapper;
        }

        public async Task<OperationResult> CreateTicketAsync(CreateTicketDto model)
        {
            using(var unitOfWork = unitOfWorkFactory.MakeUnitOfWork())
            {
                var ticket = mapper.Map<CreateTicketDto, Ticket>(model);
                await unitOfWork.Ticket.CreateAsync(ticket);
                await unitOfWork.CompleteAsync();

                return new OperationResult(true, "Задача успешно создана");
            }
        }

        public async Task<OperationResult> EditTicketAsync(CreateTicketDto model)
        {
            using(var unitOfWork = unitOfWorkFactory.MakeUnitOfWork())
            {
                var ticket = await unitOfWork.Ticket.GetByIdAsync(model.Id);
                ticket = mapper.Map(model, ticket);
                unitOfWork.Ticket.Update(ticket);
                await unitOfWork.CompleteAsync();

                return new OperationResult(true, "Задача успешно изменена");
            }
        }

        public async Task<OperationResult> DeleteTicketAsync(int id)
        {
            using(var unitOfWork = unitOfWorkFactory.MakeUnitOfWork())
            {
                var ticket = await unitOfWork.Ticket.GetByIdAsync(id);
                if(ticket.Status == StatusTask.Done)
                {
                    ticket.IsDeleted = true;
                    unitOfWork.Ticket.Update(ticket);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Задача успешно удалена");
                }
                return new OperationResult(false, "Незакрытую задачу удалить невозможно");
            }
        }

        public async Task<List<TicketDto>> GetAllTicketAsync(TicketFilterDto filter)
        {
            using(var unitOfWork = unitOfWorkFactory.MakeUnitOfWork())
            {
                var tickets = await unitOfWork.Ticket.GetAllIncludingAsync();
                var ticketViewModel = tickets.Select(mapper.Map<TicketDto>).Where(x => x.ProjectId == filter.ProjectId).ToList();
                for(int i = 0; i < ticketViewModel.Count; i++)
                {
                    SetStatusName(ticketViewModel[i]);
                }
                if(filter.Name != null)
                {
                    ticketViewModel = ticketViewModel.Where(x => x.Name.ToLower().Contains(filter.Name)).ToList();
                }
                if(filter.AuthorName != null)
                {
                    ticketViewModel = ticketViewModel.Where(x => x.AuthorName.ToLower().Contains(filter.AuthorName)).ToList();
                }
                if(filter.StatusName != null && filter.StatusName != "Все статусы")
                {
                    ticketViewModel = ticketViewModel.Where(x => x.StatusName == filter.StatusName).ToList();
                }
                if(filter.ExecutingEmployeeName != null)
                {
                    ticketViewModel = ticketViewModel.Where(x => x.ExecutingEmployeeName.ToLower().Contains(filter.ExecutingEmployeeName)).ToList();
                }
                return ticketViewModel.Where(x => x.IsDeleted == false).ToList();
            }
        }

        public async Task<TicketDto> GetTicketByIdAsync(int id)
        {
            using(var unitOfWork = unitOfWorkFactory.MakeUnitOfWork())
            {
                var ticket = await unitOfWork.Ticket.GetByIdIncludingAsync(id);
                var viewModel = mapper.Map<TicketDto>(ticket);
                SetStatusName(viewModel);
                return viewModel;
            }
        }

        private void SetStatusName(TicketDto ticket)
        {
            switch (ticket.Status)
            {
                case StatusTask.ToDo:
                    ticket.StatusName = "Сделать";
                    break;
                case StatusTask.InProgress:
                    ticket.StatusName = "В работе";
                    break;
                case StatusTask.Done:
                    ticket.StatusName = "Готово";
                    break;
            }
        }
    }
}
